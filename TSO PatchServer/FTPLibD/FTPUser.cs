﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTPLibD
{
    /// <summary>
    /// Represents a user on the FTP server.
    /// </summary>
    public class FTPUser
    {
        private string m_Username;
        private string m_Password;
        private string m_RootFolder;

        public FTPUser(string User, string Pass, string RootFolder)
        {
            m_Username = User;
            m_Password = Pass;
            m_RootFolder = RootFolder;
        }

        public string RootFolder
        {
            get { return m_RootFolder; }
            set { m_RootFolder = value; }
        }

        public string Username
        {
            get { return m_Username; }
        }

        public string Password
        {
            get { return m_Password; }
        }
    }
}
