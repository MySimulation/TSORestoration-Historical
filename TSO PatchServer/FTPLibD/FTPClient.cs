﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace FTPLibD
{
    /// <summary>
    /// A client's current transfertype.
    /// Supported types are Image (binary),
    /// ASCII (text) and EBCDIC (text).
    /// </summary>
    public enum TransferTypes
    {
        I = 0x00,   //Image (Binary)
        A = 0x01,   //ASCII (Text)
        E = 0x02    //EBCDIC (Text)
    }

    public class FTPClient
    {
        private Socket m_ClientSocket;          //The controlconnection initiated by the client.
        private Socket m_DatListenerSocket;     //For accepting a dataconnection from a client.
        private NetworkStream m_DatSenderStream;       //For sending a file to a client.
        private FTPListener m_Listener;

        private bool m_AccountIsValid = false, m_PasswordIsValid = false;
        //What kind of filetransfer this client has requested at the moment (with the TYPE command).
        private TransferTypes m_TransferType = TransferTypes.I;

        //A buffer in which to store packets that aren't fully received yet.
        protected byte[] m_RecvBuffer = new byte[11024];
        private int m_NumBytesToSend;

        private AutoResetEvent m_ConnectingWaitEvent;
        private AutoResetEvent m_FileTransferWaitEvent;

        public FTPClient(Socket ClientSock, FTPListener Server)
        {
            m_ClientSocket = ClientSock;
            m_Listener = Server;

            Logger.Initialize("FTPLibD-Log.txt");
            Logger.InfoEnabled = true;
            Logger.DebugEnabled = true;
            
            Send(Encoding.ASCII.GetBytes("220 TSO PatchServer\r\n"));

            m_ClientSocket.BeginReceive(m_RecvBuffer, 0, m_RecvBuffer.Length,
                SocketFlags.None, new AsyncCallback(OnReceivedData), m_ClientSocket);
        }

        public void Send(byte[] Data)
        {
            m_NumBytesToSend = Data.Length;
            m_ClientSocket.BeginSend(Data, 0, Data.Length, SocketFlags.None, 
                new AsyncCallback(OnSend), m_ClientSocket);
        }

        public void SendFile(byte[] FileData)
        {
            //TODO: Check a file's size, and determine what to do...
            m_FileTransferWaitEvent = new AutoResetEvent(false);

            m_NumBytesToSend = FileData.Length;
            /*m_DatSenderSocket.BeginSend(FileData, 0, FileData.Length, SocketFlags.None,
                new AsyncCallback(OnSendFile), m_DatSenderSocket);*/

            m_DatSenderStream.Write(FileData, 0, FileData.Length);
        }

        protected virtual void OnSend(IAsyncResult AR)
        {
            Socket ClientSock = (Socket)AR.AsyncState;
            int NumBytesSent = ClientSock.EndSend(AR);

            Logger.LogInfo("\r\nSent: " + NumBytesSent.ToString() + "!\r\n");

            if (NumBytesSent < m_NumBytesToSend)
                Logger.LogDebug("Didn't send everything!");
        }

        private void OnSendFile(IAsyncResult AR)
        {
            Socket ClientSock = (Socket)AR.AsyncState;
            int NumBytesSent = ClientSock.EndSend(AR);

            m_FileTransferWaitEvent.Set();
            Logger.LogInfo("\r\nSent: " + NumBytesSent.ToString() + "!\r\n");

            if (NumBytesSent < m_NumBytesToSend)
                Logger.LogDebug("Didn't send everything!");
        }

        public void OnReceivedData(IAsyncResult AR)
        {
            try
            {
                int NumBytesRead = m_ClientSocket.EndReceive(AR);

                Logger.LogInfo("Received: " + NumBytesRead.ToString() + " bytes!\r\n");

                if (Encoding.ASCII.GetString(m_RecvBuffer).Contains("\r\n"))
                {
                    m_Listener.OnReceivedData(Encoding.ASCII.GetString(m_RecvBuffer).Remove(NumBytesRead), this);
                }

                //TODO: Apply more checks here!

                m_ClientSocket.BeginReceive(m_RecvBuffer, 0, m_RecvBuffer.Length,
                    SocketFlags.None, new AsyncCallback(OnReceivedData), m_ClientSocket);
            }
            catch (SocketException)
            {
                m_ClientSocket.Shutdown(SocketShutdown.Both);
                m_ClientSocket.Disconnect(false);

                if (m_Listener != null)
                    m_Listener.RemoveClient(this);
            }
        }

        public bool OpenDataPort(string Port)
        {
            IPHostEntry HostEntry = Dns.GetHostByName(Dns.GetHostName());
            IPAddress Address = HostEntry.AddressList[0];
            Logger.LogDebug("DataAddress: " + Address.ToString());
            IPEndPoint LocalEP = new IPEndPoint(Address, int.Parse(Port));

            m_DatListenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_ConnectingWaitEvent = new AutoResetEvent(false);

            try
            {
                m_DatListenerSocket.Bind(LocalEP);
                //Don't need a high backlog, since we'll only be listening for one incoming connection.
                m_DatListenerSocket.Listen(10);
                m_DatListenerSocket.BeginAccept(new AsyncCallback(OnAcceptDataconnection), m_DatListenerSocket);
            }
            catch (SocketException)
            {
                return false;
            }

            return true;
        }

        public void CloseDataConnection(int Timeout)
        {
            /*m_DatListenerSocket.Shutdown(SocketShutdown.Both);
            m_DatListenerSocket.Close();*/
            m_DatSenderStream.Close(Timeout);
        }

        private void OnAcceptDataconnection(IAsyncResult AR)
        {
            Socket AcceptedSocket = m_DatListenerSocket.EndAccept(AR);

            if (AcceptedSocket != null)
            {
                //AcceptedSocket.Blocking = false;
                m_DatSenderStream = new NetworkStream(AcceptedSocket);
                m_ConnectingWaitEvent.Set();
            }
        }

        /// <summary>
        /// Has this client's AccountName been authenticated (with the USER command)?
        /// </summary>
        public bool AccountNameIsValid
        {
            get { return m_AccountIsValid; }
            set { m_AccountIsValid = value; }
        }

        /// <summary>
        /// Has this client's Password been authenticated (with the PASS command)?
        /// </summary>
        public bool PasswordIsValid
        {
            get { return m_PasswordIsValid; }
            set { m_PasswordIsValid = value; }
        }

        /// <summary>
        /// What is this client's current transfertype (TYPE command)?
        /// </summary>
        public TransferTypes CurrentTransferType
        {
            get { return m_TransferType; }
            set { m_TransferType = value; }
        }

        /// <summary>
        /// An AutoResetEvent that is Set() when a client
        /// connects to a newly opened dataport.
        /// </summary>
        public AutoResetEvent ConnectionResetEvent
        {
            get { return m_ConnectingWaitEvent; }
        }

        public AutoResetEvent FileTransferResetEvent
        {
            get { return m_FileTransferWaitEvent; }
        }
    }
}
