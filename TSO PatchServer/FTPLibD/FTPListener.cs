﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace FTPLibD
{
    public delegate void OnFTPReceiveDelegate(string FTPCommand, FTPClient Client);

    public class FTPListener
    {
        private ArrayList m_FTPClients;
        private Socket m_ListenerSock;
        private IPEndPoint m_LocalEP;
        private string m_IP = "";

        public event OnFTPReceiveDelegate OnReceiveEvent;

        public FTPListener()
        {
            m_ListenerSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_FTPClients = ArrayList.Synchronized(new ArrayList());
        }

        public void Initialize(int Port)
        {
            IPHostEntry HostEntry = Dns.GetHostByName(Dns.GetHostName());
            IPAddress Address = HostEntry.AddressList[0];/*System.Net.IPAddress.Any;*/
            IPEndPoint LocalEP = new IPEndPoint(Address, Port);

            m_IP = Address.ToString();

            m_LocalEP = LocalEP;

            try
            {
                m_ListenerSock.Bind(LocalEP);
                m_ListenerSock.Listen(10000);

                Console.WriteLine("Started listening on: " + LocalEP.Address.ToString()
                    + ":" + LocalEP.Port);
            }
            catch (SocketException E)
            {
                Console.WriteLine("Winsock error caused by call to Socket.Bind(): \n" + E.ToString());
            }

            m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
        }

        public void Initialize(int Port, string IP)
        {
            IPEndPoint LocalEP = new IPEndPoint(System.Net.IPAddress.Parse(IP), Port);

            m_IP = IP;

            m_LocalEP = LocalEP;

            try
            {
                m_ListenerSock.Bind(LocalEP);
                m_ListenerSock.Listen(10000);

                Console.WriteLine("Started listening on: " + LocalEP.Address.ToString()
                    + ":" + LocalEP.Port);
            }
            catch (SocketException E)
            {
                //Console.WriteLine("Winsock error caused by call to Socket.Bind(): \n" + E.ToString());
                throw E;
            }

            m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
        }

        public void OnAccept(IAsyncResult AR)
        {
            Socket AcceptedSocket = m_ListenerSock.EndAccept(AR);

            if (AcceptedSocket != null)
            {
                Logger.LogInfo("\nNew client connected!");

                //Let sockets linger for 5 seconds after they're closed, in an attempt to make sure all
                //pending data is sent!
                AcceptedSocket.LingerState = new LingerOption(true, 5);
                //AcceptedSocket.NoDelay = false;
                FTPClient NewClient = new FTPClient(AcceptedSocket, this);
                m_FTPClients.Add(NewClient);

                m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
            }
        }

        /// <summary>
        /// Called by FTPClient instances
        /// when they've received some new data
        /// (a new packet). Should not be called
        /// from anywhere else.
        /// </summary>
        /// <param name="P"></param>
        public void OnReceivedData(string FTPCommand, FTPClient Client)
        {
            OnReceiveEvent(FTPCommand, Client);
        }

        /// <summary>
        /// Removes a client from the internal list of connected clients.
        /// Should really only be called internally by the FTPClient.Disconnect()
        /// method.
        /// </summary>
        /// <param name="Client">The client to remove.</param>
        public void RemoveClient(FTPClient Client)
        {
            m_FTPClients.Remove(Client);
        }

        /// <summary>
        /// The IP address that this Listener is listening on.
        /// Will be null until Initialize() has been called!
        /// </summary>
        public string IPAddress
        {
            get { return m_IP; }
        }

        /// <summary>
        /// The number of clients that are connected to this FTPListener instance.
        /// </summary>
        public int NumConnectedClients
        {
            get { return m_FTPClients.Count; }
        }
    }
}
