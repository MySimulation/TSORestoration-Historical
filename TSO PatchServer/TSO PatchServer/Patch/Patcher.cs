﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace TSO_PatchServer.Patch
{
    class Patcher
    {
        public Patcher()
        {
            CheckVersion();
        }

        /// <summary>
        /// Checks the current version of the game.
        /// If any gameclient is below this version, it needs to be updated.
        /// </summary>
        private void CheckVersion()
        {
            if (!File.Exists("Version.txt"))
            {
                GlobalSettings.Default.PatchVersion = "1.0.0.0";
                throw new NoVersionException("Version.txt not found - assuming 1.0.0.0!");
            }

            BinaryReader Reader = new BinaryReader(File.Open("Version.txt", FileMode.Open));
            GlobalSettings.Default.PatchVersion = Reader.ReadString();
            Reader.Close();
        }
    }
}
