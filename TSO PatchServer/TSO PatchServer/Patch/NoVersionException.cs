﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSO_PatchServer.Patch
{
    /// <summary>
    /// Exception that is thrown if 'Version.txt' cannot be found.
    /// </summary>
    class NoVersionException : System.Exception
    {
        public NoVersionException()
            : base()
        {
        }

        public NoVersionException(string Message)
            : base(Message)
        {
        }
    }
}
