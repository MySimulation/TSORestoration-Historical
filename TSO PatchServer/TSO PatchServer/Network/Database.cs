﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using FTPLibD;

namespace TSO_PatchServer.Network
{
    class DBAsyncObject
    {
        private FTPClient m_Client;
        private SqlCommand m_Command;
        private string m_AccountName, m_Password;

        public DBAsyncObject(string AccountName, string Password, FTPClient Client, SqlCommand Command)
        {
            m_Client = Client;
            m_Command = Command;
            m_AccountName = AccountName;
            m_Password = Password;
        }

        public string AccountName
        {
            get { return m_AccountName; }
        }

        public string Password
        {
            get { return m_Password; }
        }

        public FTPClient Client
        {
            get { return m_Client; }
        }

        public SqlCommand Cmd
        {
            get { return m_Command; }
        }
    }

    class Database
    {
        private static SqlConnection m_Connection;

        public static void Connect()
        {
            try
            {
                m_Connection = new SqlConnection("Data Source=Afr0-PC\\SQLEXPRESS;" +
                    "Initial Catalog=TSO;User Id=Afr0;Password=Prins123;Asynchronous Processing=true");
                m_Connection.Open();
            }
            catch (Exception E)
            {
                throw new NoDBConnection("Couldn't connect to database server! Reverting to flat file DB." + 
                "\n\n" + E.ToString());
            }
        }

        public static void CheckAccount(string AccountName, ref FTPClient Client)
        {
            if (m_Connection == null)
            {
                //TODO: Check if account exists in flat-file DB.
            }

            //Gets the data from both rows (AccountName & Password) 
            SqlCommand Command = new SqlCommand("SELECT AccountName, Password FROM FTPAccounts");
            Command.Connection = m_Connection;
            Command.BeginExecuteReader(new AsyncCallback(EndCheckAccount),
                new DBAsyncObject(AccountName, "", Client, Command));
        }

        private static void EndCheckAccount(IAsyncResult AR)
        {
            DBAsyncObject AsyncObject = AR.AsyncState as DBAsyncObject;
            bool FoundAccountName = false;
            bool FoundPassword = false;

            using (SqlDataReader Reader = AsyncObject.Cmd.EndExecuteReader(AR))
            {
                while (Reader.Read())
                {
                    string Result = (string)Reader[0];
                    Result = Result.Trim().Replace("\n", "");

                    if (Result == AsyncObject.AccountName)
                    {
                        FoundAccountName = true;

                        if((string)Reader[1] != "")
                            FoundPassword = true;
                    }
                }
            }

            AsyncObject.Cmd.Dispose();

            if (FoundAccountName)
            {
                if (FoundPassword)
                {
                    AsyncObject.Client.Send(Encoding.ASCII.GetBytes("331 Expected password for this account!\r\n"));
                    AsyncObject.Client.AccountNameIsValid = true;
                    return;
                }
                else
                {
                    AsyncObject.Client.Send(Encoding.ASCII.GetBytes("230 Login OK!\r\n"));
                    AsyncObject.Client.AccountNameIsValid = true;
                    AsyncObject.Client.PasswordIsValid = true;
                    return;
                }
            }
        }

        public static void CheckPassword(string Password, ref FTPClient Client)
        {
            if (m_Connection == null)
            {
                //TODO: Check if account exists in flat-file DB.
            }

            //Gets the data from both rows (AccountName & Password) 
            SqlCommand Command = new SqlCommand("SELECT Password FROM FTPAccounts");
            Command.Connection = m_Connection;
            Command.BeginExecuteReader(new AsyncCallback(EndCheckPassword),
                new DBAsyncObject("", Password, Client, Command));
        }

        private static void EndCheckPassword(IAsyncResult AR)
        {
            DBAsyncObject AsyncObject = AR.AsyncState as DBAsyncObject;
            bool FoundPassword = false;

            using (SqlDataReader Reader = AsyncObject.Cmd.EndExecuteReader(AR))
            {
                while (Reader.Read())
                {
                    string Result = (string)Reader[0];
                    Result = Result.Trim().Replace("\n", "");

                    if (Result == AsyncObject.Password)
                    {
                        FoundPassword = true;
                        break;
                    }
                }
            }

            AsyncObject.Cmd.Dispose();

            if (FoundPassword)
            {
                if (AsyncObject.Client.AccountNameIsValid)
                {
                    AsyncObject.Client.Send(Encoding.ASCII.GetBytes("230 Login OK!\r\n"));
                    AsyncObject.Client.PasswordIsValid = true;
                    return;
                }
                else
                {
                    //TODO: Send 331??
                    Logger.LogDebug("AccountName wasn't authenticated!\r\n");
                }
            }
        }
    }
}
