﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace TSO_PatchServer.Network
{
    public delegate void OnReceiveDelegate(PacketStream P, PatchClient Client);

    public class PatchListener
    {
        private ArrayList m_PatchClients;
        private Socket m_ListenerSock;
        private IPEndPoint m_LocalEP;
        private string m_IP = "";

        public event OnReceiveDelegate OnReceiveEvent;

        public PatchListener()
        {
            m_ListenerSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_PatchClients = ArrayList.Synchronized(new ArrayList());
        }

        public void Initialize(int Port)
        {
            IPHostEntry HostEntry = Dns.GetHostByName(Dns.GetHostName());
            IPAddress Address = HostEntry.AddressList[0];/*System.Net.IPAddress.Any;*/
            IPEndPoint LocalEP = new IPEndPoint(Address, Port);

            m_IP = Address.ToString();

            m_LocalEP = LocalEP;

            try
            {
                m_ListenerSock.Bind(LocalEP);
                m_ListenerSock.Listen(10000);

                Console.WriteLine("Started listening on: " + LocalEP.Address.ToString()
                    + ":" + LocalEP.Port);
            }
            catch (SocketException E)
            {
                Console.WriteLine("Winsock error caused by call to Socket.Bind(): \n" + E.ToString());
            }

            m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
        }

        public void OnAccept(IAsyncResult AR)
        {
            Socket AcceptedSocket = m_ListenerSock.EndAccept(AR);

            if (AcceptedSocket != null)
            {
                Console.WriteLine("\nNew client connected!");

                //Let sockets linger for 5 seconds after they're closed, in an attempt to make sure all
                //pending data is sent!
                AcceptedSocket.LingerState = new LingerOption(true, 5);
                PatchClient NewClient = new PatchClient(AcceptedSocket, this);
                m_PatchClients.Add(NewClient);

                m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
            }
        }

        /// <summary>
        /// Called by LoginClient instances
        /// when they've received some new data
        /// (a new packet). Should not be called
        /// from anywhere else.
        /// </summary>
        /// <param name="P"></param>
        public void OnReceivedData(PacketStream P, PatchClient Client)
        {
            OnReceiveEvent(P, Client);
        }

        /// <summary>
        /// Removes a client from the internal list of connected clients.
        /// Should really only be called internally by the LoginClient.Disconnect()
        /// method.
        /// </summary>
        /// <param name="Client">The client to remove.</param>
        public void RemoveClient(PatchClient Client)
        {
            m_PatchClients.Remove(Client);
        }

        /// <summary>
        /// The IP address that this Listener is listening on.
        /// Will be null until Initialize() has been called!
        /// </summary>
        public string IPAddress
        {
            get { return m_IP; }
        }

        /// <summary>
        /// The number of clients that are connected to this PatchListener instance.
        /// </summary>
        public int NumConnectedClients
        {
            get { return m_PatchClients.Count; }
        }
    }
}
