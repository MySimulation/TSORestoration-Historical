﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace TSO_PatchServer.Network
{
    /// <summary>
    /// Class with static helper methods.
    /// </summary>
    class Helper
    {
        /// <summary>
        /// Returns the size of a directory.
        /// </summary>
        /// <param name="d">The DirectoryInfo instance representing the directory.</param>
        /// <returns>The size of the directory, in bytes.</returns>
        public static long DirSize(DirectoryInfo d)
        {
            long Size = 0;

            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                Size += fi.Length;
            }

            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                Size += DirSize(di);
            }

            return (Size);
        }

        /// <summary>
        /// Pads out a string representation of a specific point in time (DateTime.Year, DateTime.Month,
        /// DateTime.Day, DayTime.Hour, DayTime.Minute, DayTime.Second) by adding a leading 0 if the string
        /// is only 1 character long.
        /// </summary>
        /// <param name="Time">The specific point in time to pad.</param>
        /// <returns>The padded time if the string was 1 character long, or the string that was passed in
        /// if it was more than 1 character long.</returns>
        public static string PadTime(string Time)
        {
            if (Time.Length == 1)
                return "0" + Time;

            return Time;
        }
    }
}
