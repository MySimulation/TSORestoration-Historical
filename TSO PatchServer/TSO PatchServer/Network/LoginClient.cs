﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace TSO_PatchServer.Network
{
    public delegate void OnLoginReceiveDelegate(PacketStream P, LoginClient Client);

    /// <summary>
    /// Represents a connect client (in this case, the LoginServer).
    /// </summary>
    public class LoginClient
    {
        class AsyncConnectionObject
        {
            private Socket m_Socket;
            private string m_Hostname;

            public AsyncConnectionObject(Socket Sock, string Hostname)
            {
                m_Socket = Sock;
                m_Hostname = Hostname;
            }

            public Socket ConnectionSocket
            {
                get { return m_Socket; }
            }

            public string Hostname
            {
                get { return m_Hostname; }
            }
        }

        private Socket m_Socket;
        public event OnLoginReceiveDelegate OnReceiveEvent;
        private static Dictionary<byte, int> m_PacketIDs = new Dictionary<byte, int>();

        private AutoResetEvent m_ConnectingWaitEvent;

        /// <summary>
        /// An AutoResetEvent instance that will get Set() once
        /// this LoginClient instance has finished connecting.
        /// Use System.Threading.WaitHandle to Wait() for it.
        /// </summary>
        public AutoResetEvent ConnectingWaitEvent
        {
            get { return m_ConnectingWaitEvent; }
        }

        //A buffer in which to store packets that aren't fully received yet.
        protected byte[] m_RecvBuffer = new byte[11024];
        //Buffer for storing packets that were not fully read.
        private PacketStream m_TempPacket;

        //The number of bytes to be sent. See Send()
        private int m_NumBytesToSend = 0;

                /// <summary>
        /// Constructor to be used when connecting to a remote host (server).
        /// </summary>
        /// <param name="IP">The IP of the remote host.</param>
        /// <param name="Port">The Port of the remote host.</param>
        /// <param name="HostName">The name of the remote host, mainly for exception information purposes.</param>
        public LoginClient(string IP, int Port, string HostName)
        {
            m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            m_PacketIDs = new Dictionary<byte, int>();

            m_ConnectingWaitEvent = new AutoResetEvent(false);

            m_Socket.BeginConnect(new IPEndPoint(IPAddress.Parse(IP), Port), 
                new AsyncCallback(OnConnect), new AsyncConnectionObject(m_Socket, HostName));
        }

        /// <summary>
        /// Standard callback for asynchronously connecting to a remote host.
        /// </summary>
        private void OnConnect(IAsyncResult AR)
        {
            AsyncConnectionObject ConnectionObj = (AsyncConnectionObject)AR.AsyncState;

            try
            {
                ConnectionObj.ConnectionSocket.EndConnect(AR);
                m_ConnectingWaitEvent.Set();

                m_Socket.BeginReceive(m_RecvBuffer, 0, m_RecvBuffer.Length, SocketFlags.None,
                    new AsyncCallback(OnReceivedData), m_Socket);
            }
            catch (SocketException)
            {
                MessageBox.Show("Error while trying to connect to: " + ConnectionObj.Hostname + 
                    "! \nShutting down...");
                Environment.Exit(0);
            }
        }

        public void OnReceivedData(IAsyncResult AR)
        {
            try
            {
                int NumBytesRead = m_Socket.EndReceive(AR);

                Logger.LogInfo("Received: " + NumBytesRead.ToString() + " bytes!");

                //The packet is given an ID of 0x00 because its ID is currently unknown.
                PacketStream TempPacket = new PacketStream(0x00, NumBytesRead, m_RecvBuffer);
                byte ID = TempPacket.PeekByte(0);
                int PacketLength = 0;

                bool FoundMatchingID = false;

                FoundMatchingID = FindMatchingPacketID(ID);

                if (FoundMatchingID)
                {
                    PacketLength = m_PacketIDs[ID];

                    if (NumBytesRead == PacketLength)
                    {
                        OnReceiveEvent(new PacketStream(ID, PacketLength, TempPacket.ToArray()), this);
                        m_RecvBuffer = new byte[11024];
                    }
                    else if (NumBytesRead < PacketLength)
                    {
                        m_TempPacket = new PacketStream(ID, PacketLength);
                        byte[] TmpBuffer = new byte[NumBytesRead];

                        //Store the number of bytes that were read in the temporary buffer.
                        Buffer.BlockCopy(m_RecvBuffer, 0, TmpBuffer, 0, NumBytesRead);
                        m_TempPacket.WriteBytes(TmpBuffer);

                        //And reset the buffers!
                        m_RecvBuffer = new byte[11024];
                        TmpBuffer = null;
                    }
                }
                else
                {
                    if (m_TempPacket != null)
                    {
                        if (m_TempPacket.Length < m_TempPacket.BufferLength)
                        {
                            //Received the exact number of bytes needed to complete the stored packet.
                            if ((m_TempPacket.BufferLength + NumBytesRead) == m_TempPacket.Length)
                            {
                                byte[] TmpBuffer = new byte[NumBytesRead];
                                Buffer.BlockCopy(m_RecvBuffer, 0, TmpBuffer, 0, NumBytesRead);

                                m_RecvBuffer = new byte[11024];
                                TmpBuffer = null;
                            }
                            //Received more than the number of bytes needed to complete the packet!
                            else if ((m_TempPacket.BufferLength + NumBytesRead) > m_TempPacket.Length)
                            {
                                int Target = (int)((m_TempPacket.BufferLength + NumBytesRead) - m_TempPacket.Length);
                                byte[] TmpBuffer = new byte[Target];

                                Buffer.BlockCopy(m_RecvBuffer, 0, TmpBuffer, 0, Target);
                                m_TempPacket.WriteBytes(TmpBuffer);

                                //Now we have a full packet, so call the received event!
                                OnReceiveEvent(new PacketStream(m_TempPacket.PacketID,
                                    (int)m_TempPacket.Length, m_TempPacket.ToArray()), this);

                                //Copy the remaining bytes in the receiving buffer.
                                TmpBuffer = new byte[NumBytesRead - Target];
                                Buffer.BlockCopy(m_RecvBuffer, Target, TmpBuffer, 0, (NumBytesRead - Target));

                                //Give the temporary packet an ID of 0x00 since we don't know its ID yet.
                                TempPacket = new PacketStream(0x00, NumBytesRead - Target, TmpBuffer);
                                ID = TempPacket.PeekByte(0);

                                //This SHOULD be an existing ID, but let's sanity-check it...
                                if (FindMatchingPacketID(ID))
                                {
                                    m_TempPacket = new PacketStream(ID, m_PacketIDs[ID], TempPacket.ToArray());

                                    //Congratulations, you just received another packet!
                                    if (m_TempPacket.Length == m_TempPacket.BufferLength)
                                    {
                                        OnReceiveEvent(new PacketStream(m_TempPacket.PacketID,
                                            (int)m_TempPacket.Length, m_TempPacket.ToArray()), this);

                                        //No more data to store on this read, so reset everything...
                                        m_TempPacket = null;
                                        TmpBuffer = null;
                                        m_RecvBuffer = new byte[11024];
                                    }
                                }
                                else
                                {
                                    //Houston, we have a problem (this should never occur)!
                                }
                            }
                        }
                    }
                }

                m_Socket.BeginReceive(m_RecvBuffer, 0, m_RecvBuffer.Length, SocketFlags.None,
                    new AsyncCallback(OnReceivedData), m_Socket);
            }
            catch (SocketException)
            {
                Disconnect();
            }
        }

        public void Send(byte[] Data)
        {
            m_NumBytesToSend = Data.Length;
            m_Socket.BeginSend(Data, 0, Data.Length, SocketFlags.None, new AsyncCallback(OnSend), m_Socket);
        }

        protected virtual void OnSend(IAsyncResult AR)
        {
            Socket ClientSock = (Socket)AR.AsyncState;
            int NumBytesSent = ClientSock.EndSend(AR);

            Logger.LogInfo("Sent: " + NumBytesSent.ToString() + "!");

            if (NumBytesSent < m_NumBytesToSend)
                Logger.LogDebug("Didn't send everything!");
        }

        /// <summary>
        /// Disconnects this LoginClient instance and stops
        /// all sending and receiving of data.
        /// </summary>
        public void Disconnect()
        {
            m_Socket.Shutdown(SocketShutdown.Both);
            m_Socket.Disconnect(false);
        }

        private bool FindMatchingPacketID(byte ID)
        {
            foreach (KeyValuePair<byte, int> Pair in m_PacketIDs)
            {
                if (ID == Pair.Key)
                {
                    Console.WriteLine("Found matching Packet ID!");

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Register a Packet ID with a corresponding Packet Length from a specific protocol.
        /// </summary>
        /// <param name="ID">The ID to register.</param>
        /// <param name="Length">The length of the packet to register.</param>
        public static void RegisterLoginPacketID(byte ID, int Length)
        {
            m_PacketIDs.Add(ID, Length);
        }
    }
}
