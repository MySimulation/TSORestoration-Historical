# TSO Restoration
TSOR (The Sims Online Restoration) was the first step in recreating The Sims Online using the XNA framework. The code would later form a foundation for to create Project Dollhouse and later FreeSO. Unlike the latter two, TSOR used XNA 3 and is incompitable with MonoGame.

While TSOR is fully capable of being built, it's not playable in any form and will only get you to the login screen that barely resemables the original. You're free to do whatever you want with it but sell it.

## Build requirements

* XNA 3.1 Game Studio
* BASS.Net
* Visual Studio 2008 Express