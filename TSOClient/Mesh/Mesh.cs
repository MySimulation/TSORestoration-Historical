﻿using System;
using System.Collections.Generic;
using DNA;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;

namespace MeshUtils
{
    public struct Face
    {
        public int X, Y, Z;
    }

    public struct Vertex
    {
        public Vector3 pos;
        public Vector3 normal;
        public Vector2 uv;

        public Vertex(float[] pos_v, float[] norm_v, float[] uv_v)
        {
            pos = new Vector3(pos_v[0], pos_v[1], pos_v[2]);
            normal = new Vector3(norm_v[0], norm_v[1], norm_v[2]);
            uv = new Vector2(uv_v[0], uv_v[1]);
        }
    }

    public class Mesh
    {
        private static int m_Version = 0;

        private static int m_BoneCount = 0;
        private static List<string> m_BoneNames = new List<string>();

        private static int m_FaceCount = 0;
        private static Face[] m_Faces;

        private static int m_BndCount = 0;
        private static int[,] m_BoneBindings;

        private static int m_NumTexVerticies = 0;
        private static Single[,] m_TexVerticies;

        private static int m_BlendCount = 0;
        private static int[,] m_BlendData;

        private static int m_VertexCount = 0;
        private static Single[,] m_VertexData;

        public Vertex[] SerializeVertices()
        {
            Vertex[] serialized = new Vertex[m_FaceCount * 3];

            int position = 0;
            foreach (Face f in m_Faces)
            {
                serialized[position++] = new Vertex(new float[] { m_VertexData[f.X, 0], m_VertexData[f.X, 1], m_VertexData[f.X, 2] }, new float[] { m_VertexData[f.X, 3], m_VertexData[f.X, 4], m_VertexData[f.X, 5] }, new float[] { m_TexVerticies[f.X, 1], Math.Abs(1 - m_TexVerticies[f.X, 2]) });
                serialized[position++] = new Vertex(new float[] { m_VertexData[f.Y, 0], m_VertexData[f.Y, 1], m_VertexData[f.Y, 2] }, new float[] { m_VertexData[f.Y, 3], m_VertexData[f.Y, 4], m_VertexData[f.Y, 5] }, new float[] { m_TexVerticies[f.Y, 1], Math.Abs(1 - m_TexVerticies[f.Y, 2]) });
                serialized[position++] = new Vertex(new float[] { m_VertexData[f.Z, 0], m_VertexData[f.Z, 1], m_VertexData[f.Z, 2] }, new float[] { m_VertexData[f.Z, 3], m_VertexData[f.Z, 4], m_VertexData[f.Z, 5] }, new float[] { m_TexVerticies[f.Z, 1], Math.Abs(1 - m_TexVerticies[f.Z, 2]) });
            }

            return serialized;
        }

        public int VertexCount { get { return m_FaceCount * 3; } }

        public Mesh(Stream stream)
        {
            BinaryReader Reader = new BinaryReader(stream);

            m_Version = Endian.SwapInt32(Reader.ReadInt32());
            Console.WriteLine("Version: " + m_Version + "\n");

            m_BoneCount = Endian.SwapInt32(Reader.ReadInt32());
            Console.WriteLine("Number of bones: " + m_BoneCount);

            for (int i = 0; i < m_BoneCount; i++)
            {
                byte StrLen = Reader.ReadByte();
                string BoneName = Encoding.ASCII.GetString(Reader.ReadBytes(StrLen));
                m_BoneNames.Add(BoneName);
                Console.WriteLine(BoneName);
            }

            m_FaceCount = Endian.SwapInt32(Reader.ReadInt32());
            m_Faces = new Face[m_FaceCount];
            Console.WriteLine("Number of faces: " + m_FaceCount);

            for (int i = 0; i < m_FaceCount; i++)
            {
                m_Faces[i].X = Endian.SwapInt32(Reader.ReadInt32());
                m_Faces[i].Y = Endian.SwapInt32(Reader.ReadInt32());
                m_Faces[i].Z = Endian.SwapInt32(Reader.ReadInt32());
            }

            m_BndCount = Endian.SwapInt32(Reader.ReadInt32());
            m_BoneBindings = new int[m_BndCount, 5];
            Console.WriteLine("Number of bonebindings: " + m_BndCount);

            for (int i = 0; i < m_BndCount; i++)
                for (int j = 0; j < 5; j++)
                    m_BoneBindings[i, j] = Endian.SwapInt32(Reader.ReadInt32());

            m_NumTexVerticies = Endian.SwapInt32(Reader.ReadInt32());
            m_TexVerticies = new Single[m_NumTexVerticies, 3];
            Console.WriteLine("Number of texture verticies: " + m_NumTexVerticies);

            switch (m_Version)
            {
                case 0:
                    for (int i = 0; i < m_NumTexVerticies; i++)
                    {
                        //These coordinates aren't reversed, and the Endian class
                        //doesn't support swapping Single values, so do it manually...
                        m_TexVerticies[i, 0] = i;
                        byte[] XOffset = Reader.ReadBytes(4);
                        byte[] YOffset = Reader.ReadBytes(4);

                        Array.Reverse(XOffset);
                        Array.Reverse(YOffset);

                        m_TexVerticies[i, 1] = BitConverter.ToSingle(XOffset, 0);
                        m_TexVerticies[i, 2] = BitConverter.ToSingle(YOffset, 0);
                    }

                    break;
                default:
                    for (int i = 0; i < m_NumTexVerticies; i++)
                    {
                        m_TexVerticies[i, 0] = i;
                        m_TexVerticies[i, 1] = Reader.ReadSingle(); //X offset
                        m_TexVerticies[i, 2] = Reader.ReadSingle(); //Y offset
                    }

                    break;
            }

            m_BlendCount = Endian.SwapInt32(Reader.ReadInt32());
            m_BlendData = new int[m_BlendCount, 2];
            Console.WriteLine("Number of blends: " + m_BlendCount);

            for (int i = 0; i < m_BlendCount; i++)
            {
                m_BlendData[i, 1] = Endian.SwapInt32(Reader.ReadInt32());
                m_BlendData[i, 0] = Endian.SwapInt32(Reader.ReadInt32());
            }

            m_VertexCount = Endian.SwapInt32(Reader.ReadInt32());
            m_VertexData = new Single[m_VertexCount, 7];
            Console.WriteLine("Number of verticies: " + m_VertexCount);

            switch (m_Version)
            {
                case 0:
                    for (int i = 0; i < m_VertexCount; i++)
                    {
                        m_VertexData[i, 0] = i;

                        for (int j = 0; j < 6; j++)
                            m_VertexData[i, j] = Reader.ReadSingle();
                    }

                    break;
                default:
                    for (int i = 0; i < m_VertexCount; i++)
                    {
                        m_VertexData[i, 0] = i;

                        //These coordinates are apparently reversed, but since the file is Big-Endian,
                        //and the default is reading Little-Endian, there should be no need to convert...
                        for (int j = 0; j < 6; j++)
                            m_VertexData[i, j] = Reader.ReadSingle();
                    }

                    break;
            }

            //Console.ReadLine();
        }

        public Stream ToSMD()
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter sw = new StreamWriter(stream);

            sw.WriteLine("version 1");
            sw.WriteLine("nodes");
            int i = 0;
            foreach (string s in m_BoneNames)
            {
                sw.WriteLine(i++ + " \"" + s + "\" " + "-1");
            }
            sw.WriteLine("end");
            sw.WriteLine("skeleton");
            sw.WriteLine("time 0");
            for (int j = 0; j < i; j++)
            {
                sw.WriteLine(j + " 0 0 0 0 0 0");
            }
            sw.WriteLine("end");
            sw.WriteLine("triangles");
            foreach (Face f in m_Faces)
            {
                sw.WriteLine("default.bmp");
                sw.WriteLine("" + 0 + ' ' + m_VertexData[f.X, 0] + ' ' + m_VertexData[f.X, 1] + ' ' + m_VertexData[f.X, 2] + ' ' + m_VertexData[f.X, 3] + ' ' + m_VertexData[f.X, 4] + ' ' + m_VertexData[f.X, 5] + ' ' + m_TexVerticies[f.X, 1] + ' ' + Math.Abs(1-m_TexVerticies[f.X, 2]));
                sw.WriteLine("" + 0 + ' ' + m_VertexData[f.Y, 0] + ' ' + m_VertexData[f.Y, 1] + ' ' + m_VertexData[f.Y, 2] + ' ' + m_VertexData[f.Y, 3] + ' ' + m_VertexData[f.Y, 4] + ' ' + m_VertexData[f.Y, 5] + ' ' + m_TexVerticies[f.Y, 1] + ' ' + Math.Abs(1 - m_TexVerticies[f.Y, 2]));
                sw.WriteLine("" + 0 + ' ' + m_VertexData[f.Z, 0] + ' ' + m_VertexData[f.Z, 1] + ' ' + m_VertexData[f.Z, 2] + ' ' + m_VertexData[f.Z, 3] + ' ' + m_VertexData[f.Z, 4] + ' ' + m_VertexData[f.Z, 5] + ' ' + m_TexVerticies[f.Z, 1] + ' ' + Math.Abs(1 - m_TexVerticies[f.Z, 2]));
            }
            sw.WriteLine("end");

            return stream;
        }
    }
}
