﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Microsoft.Xna.Framework.Graphics;
using System.IO;

namespace TSOClient
{
    public class Floor : GameObject
    {
        string myFloorName;
        string myFloorDesc;
        Texture2D[] myFloorLods;
        string mySpriteName;
        string myFloorPrice;
        

        public string FloorName { get { return myFloorName; } }
        public string FloorDesc { get { return myFloorDesc; } }
        public string SpriteName { get { return mySpriteName; } }
        public string FloorPrice { get { return myFloorPrice; } }
        public int Width { get { return myFloorLods[DrawSize].Width; } }
        public int Height { get { return myFloorLods[DrawSize].Height; } }
        

        public Floor(string name, string price, string desc, Bitmap[] lods, GraphicsDevice gd, string spriteName)
        {
            myFloorName = name;
            myFloorDesc = desc;
            myFloorPrice = price;
            mySpriteName = spriteName;
            myFloorLods = new Texture2D[3];

            for (int i = 0; i < 3; i++)
            {
                MemoryStream ms = new MemoryStream();
                Microsoft.Xna.Framework.Graphics.Color[] pixels = new Microsoft.Xna.Framework.Graphics.Color[lods[i].Height * lods[i].Width];
                for (int j = 0; j < lods[i].Height; j++)
                {
                    for (int k = 0; k < lods[i].Width; k++)
                    {
                        System.Drawing.Color currentColor = lods[i].GetPixel(k, j);
                        pixels[j * lods[i].Width + k] = new Microsoft.Xna.Framework.Graphics.Color(currentColor.R, currentColor.G, currentColor.B, currentColor.A);
                    }
                }

                myFloorLods[i] = new Texture2D(gd, lods[i].Width, lods[i].Height);
                myFloorLods[i].SetData<Microsoft.Xna.Framework.Graphics.Color>(pixels);
            }
        }

        public override void Draw(SpriteBatch SBatch)
        {
            SBatch.Draw(myFloorLods[DrawSize], new Microsoft.Xna.Framework.Rectangle(ScreenPosition[0] + GameObject.GlobalXTranslation, ScreenPosition[1] + GameObject.GlobalYTranslation, myFloorLods[DrawSize].Width, myFloorLods[DrawSize].Height), Microsoft.Xna.Framework.Graphics.Color.White);
        }

        public override string ToString()
        {
            return myFloorName;
        }
    }
}
