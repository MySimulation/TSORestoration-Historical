﻿using System;
using System.Collections.Generic;
using System.Text;
using SimsLib.FAR3;
using DNA;
using System.IO;
using Iffinator.Flash;
using Microsoft.Xna.Framework.Graphics;
using SimsLib.FAR1;
using System.Drawing;

namespace TSOClient
{
    public class ContentManager
    {
        private static Dictionary<ulong, string> m_Resources;
        private static bool initComplete = false;
        private static Random m_Rand;
        private static Dictionary<string, FAR3Archive> m_Archives;
        private static List<Floor> m_Floors;

        public static List<Floor> Floors { get { return m_Floors; } }
        
        static ContentManager()
        {
            m_Archives = new Dictionary<string, FAR3Archive>();
            m_Rand = new Random(0);
            FAR3Archive packingSlips = new FAR3Archive(GlobalSettings.Default.StartupPath + "packingslips\\packingslips.dat");

            List<KeyValuePair<uint, byte[]>> entries = packingSlips.GetAllEntries();

            m_Resources = new Dictionary<ulong, string>();
            foreach (KeyValuePair<uint, byte[]> kvp in entries)
            {
                BinaryReader br = new BinaryReader(new MemoryStream(kvp.Value));
                br.BaseStream.Position = 50;
                string path = br.ReadString();
                br.BaseStream.Position += 8;
                ulong id = Endian.SwapUInt64(br.ReadUInt64());
                m_Resources.Add(id, path);
            }
            KeyValuePair<uint, byte[]> vp = entries[35854];
            string s = new List<string>(m_Resources.Values)[35854];
            initComplete = true;
        }

        public static byte[] GetResourceFromLongID(ulong id)
        {
            while (!initComplete) ;
            if (m_Resources.ContainsKey(id))
            {
                string path = m_Resources[id].Replace("./", "");
                if (!File.Exists(path))
                {
                    string[] pathSections = path.Split(new char[] { '/' });
                    int directoryIdx = 0;
                    if (path.Contains("/heads/") || path.Contains("/hands/") || path.Contains("/bodies/") || path.Contains("/accessories/"))
                        directoryIdx = Array.FindLastIndex<string>(pathSections, delegate(string it) { if (it.CompareTo("avatardata") == 0) { return true; } return false; }) + 2;
                    else
                        directoryIdx = Array.FindLastIndex<string>(pathSections, delegate(string it) { if (it.CompareTo("TSOClient") == 0) { return true; } return false; }) + 2;
                    string directoryName = pathSections[directoryIdx];
                    path = path.Remove(path.LastIndexOf('/'));
                    string archivePath = GlobalSettings.Default.StartupPath + '/' + path.Remove((path.LastIndexOf(pathSections[directoryIdx]))) + directoryName + '/' + directoryName + ".dat";

                    if (!m_Archives.ContainsKey(archivePath))
                    {
                        FAR3Archive archive = new FAR3Archive(archivePath);
                        m_Archives.Add(archivePath, archive);
                        return archive[pathSections[pathSections.Length - 1]];
                    }
                    else
                    {
                        return m_Archives[archivePath].GetItemByID((uint)(id>>32));
                    }
                }
                else
                {
                    return File.ReadAllBytes(GlobalSettings.Default.StartupPath + path);
                }
            }
            return new byte[0];
        }

        public byte[] this[ulong id]
        {
            get
            {
                if (m_Resources.ContainsKey(id))
                {
                    string path = m_Resources[id].Replace("./", "");
                    if (!File.Exists(path))
                    {
                        string[] pathSections = path.Split(new char[] { '/' });
                        string directoryName = pathSections[pathSections.Length - 2];
                        string archivePath = GlobalSettings.Default.StartupPath + path.Remove(path.LastIndexOf('/') + 1) + directoryName + ".dat";

                        FAR3Archive archive = new FAR3Archive(archivePath);
                        return archive[pathSections[pathSections.Length - 1]];
                    }
                    else
                    {
                        return File.ReadAllBytes(GlobalSettings.Default.StartupPath + path);
                    }
                }
                return new byte[0];
            }
        }

        public static void InitFloors(GraphicsDevice gd)
        {
            m_Floors = new List<Floor>();

            string floors1dat = GlobalSettings.Default.StartupPath + "housedata\\floors\\floors.far";
            string floors2dat = GlobalSettings.Default.StartupPath + "housedata\\floors2\\floors2.far";
            string floors3dat = GlobalSettings.Default.StartupPath + "housedata\\floors3\\floors3.far";
            string floors4dat = GlobalSettings.Default.StartupPath + "housedata\\floors4\\floors4.far";
            string floors5iff = GlobalSettings.Default.StartupPath + "objectdata\\globals\\floors.iff";
            string floors5Namesiff = GlobalSettings.Default.StartupPath + "objectdata\\globals\\build.iff";

            FARArchive floors1 = new FARArchive(floors1dat);
            FARArchive floors2 = new FARArchive(floors2dat);
            FARArchive floors3 = new FARArchive(floors3dat);
            FARArchive floors4 = new FARArchive(floors4dat);
            Iff floors5        = new Iff       (File.OpenRead(floors5iff));
            Iff floors5names   = new Iff       (File.OpenRead(floors5Namesiff));

            // Load the first floor data
            List<KeyValuePair<string, byte[]>> floors = floors1.GetAllEntries();
            foreach (KeyValuePair<string, byte[]> kvp in floors)
            {
                Iff flr = new Iff(new MemoryStream(kvp.Value));
                List<StringTableString> catalogInfo = (flr.StringTables[0].StringSets.Count > 0) ? flr.StringTables[0].StringSets[0].Strings : flr.StringTables[0].Strings;

                m_Floors.Add(new Floor(catalogInfo[0].Str, catalogInfo[1].Str, catalogInfo[2].Str, new System.Drawing.Bitmap[] {
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 1; }).GetFrame(0).BitmapData, 
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 257; }).GetFrame(0).BitmapData,
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 513; }).GetFrame(0).BitmapData},
                    gd, flr.Sprites.Find(delegate(SpriteParser sp) { return sp.ChunkID == 1; }).Name));
            }

            // Load the second floor data
            floors = floors2.GetAllEntries();
            foreach (KeyValuePair<string, byte[]> kvp in floors)
            {
                Iff flr = new Iff(new MemoryStream(kvp.Value));
                List<StringTableString> catalogInfo = (flr.StringTables[0].StringSets.Count > 0) ? flr.StringTables[0].StringSets[0].Strings : flr.StringTables[0].Strings;

                m_Floors.Add(new Floor(catalogInfo[0].Str, catalogInfo[1].Str, catalogInfo[2].Str, new System.Drawing.Bitmap[] {
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 1; }).GetFrame(0).BitmapData, 
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 257; }).GetFrame(0).BitmapData,
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 513; }).GetFrame(0).BitmapData},
                    gd, flr.Sprites.Find(delegate(SpriteParser sp) { return sp.ChunkID == 1; }).Name));
            }

            // Load the third floor data
            floors = floors3.GetAllEntries();
            foreach (KeyValuePair<string, byte[]> kvp in floors)
            {
                Iff flr = new Iff(new MemoryStream(kvp.Value));
                List<StringTableString> catalogInfo = (flr.StringTables[0].StringSets.Count > 0) ? flr.StringTables[0].StringSets[0].Strings : flr.StringTables[0].Strings;

                m_Floors.Add(new Floor(catalogInfo[0].Str, catalogInfo[1].Str, catalogInfo[2].Str, new System.Drawing.Bitmap[] {
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 1; }).GetFrame(0).BitmapData, 
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 257; }).GetFrame(0).BitmapData,
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 513; }).GetFrame(0).BitmapData},
                    gd, flr.Sprites.Find(delegate(SpriteParser sp) { return sp.ChunkID == 1; }).Name));
            }

            // Load the fourth floor data
            floors = floors4.GetAllEntries();
            foreach (KeyValuePair<string, byte[]> kvp in floors)
            {
                Iff flr = new Iff(new MemoryStream(kvp.Value));
                List<StringTableString> catalogInfo = (flr.StringTables[0].StringSets.Count > 0) ? flr.StringTables[0].StringSets[0].Strings : flr.StringTables[0].Strings;

                m_Floors.Add(new Floor(catalogInfo[0].Str, catalogInfo[1].Str, catalogInfo[2].Str, new System.Drawing.Bitmap[] {
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 1; }).GetFrame(0).BitmapData, 
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 257; }).GetFrame(0).BitmapData,
                    flr.Sprites.Find(delegate (SpriteParser sp) { return sp.ChunkID == 513; }).GetFrame(0).BitmapData},
                    gd, flr.Sprites.Find(delegate(SpriteParser sp) { return sp.ChunkID == 1; }).Name));
            }

            for (int i = 1; i < 30; i++)
            {
                Bitmap[] frames = new Bitmap[3];

                string spriteName = floors5.Sprites[i].Name;
                frames[0] = floors5.Sprites.Find(delegate(SpriteParser sp) { return sp.ChunkID == i; }).GetFrame(0).BitmapData;
                frames[1] = floors5.Sprites.Find(delegate(SpriteParser sp) { return sp.ChunkID == i + 256; }).GetFrame(0).BitmapData;
                frames[2] = floors5.Sprites.Find(delegate(SpriteParser sp) { return sp.ChunkID == i + 512; }).GetFrame(0).BitmapData;

                string price = floors5names.StringTables[2].StringSets[0].Strings[(i - 1) * 3].Str;
                string title = floors5names.StringTables[2].StringSets[0].Strings[(i - 1) * 3 + 1].Str;
                string description = floors5names.StringTables[2].StringSets[0].Strings[(i - 1) * 3 + 2].Str;

                m_Floors.Add(new Floor(title, price, description, frames, gd, spriteName));
            }

            //m_Floors.Sort(delegate(Floor f1, Floor f2) { return f1.FloorName.CompareTo(f2.FloorName); });
        }
    }
}
