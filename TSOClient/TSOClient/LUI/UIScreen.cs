﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using SimsLib.FAR3;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace TSOClient.LUI
{
    public class UIScreen
    {
        private ScreenManager m_ScreenMgr;
        private MouseState m_CurrentMouseState;
        private MouseState m_PreviousMouseState;

        private List<UIElement> m_UIElements = new List<UIElement>();
        private List<NetworkedUIElement> m_NetUIElements = new List<NetworkedUIElement>();
        private List<Texture2D> m_Backgrounds = new List<Texture2D>();
        private List<ImgInfoPopup> m_Popups = new List<ImgInfoPopup>();

        public ScreenManager ScreenMgr
        {
            get { return m_ScreenMgr; }
        }

        public UIScreen(ScreenManager ScreenMgr)
        {
            m_ScreenMgr = ScreenMgr;
        }

        public void HighlightButton(string strID)
        {
            UIElement elem = m_UIElements.Find(delegate(UIElement element) { return element.StrID.CompareTo(strID) == 0; });
            if (elem is UIButton)
                ((UIButton)elem).CurrentFrame = 1;
            
        }

        #region Construction methods

        public void LoadBackground(uint id_0, uint id_1, string TextureName, string source)
        {
            ulong ID = (ulong)(((ulong)id_0)<<32 | ((ulong)(id_1 >> 32)));
            if (ID != 0x00)
            {
                MemoryStream TextureStream = new MemoryStream(ContentManager.GetResourceFromLongID(ID));
                m_Backgrounds.Add(Texture2D.FromFile(m_ScreenMgr.GraphicsDevice, TextureStream));
            }
            else
            {
                Texture2D Background = m_ScreenMgr.GameComponent.Content.Load<Texture2D>(TextureName);
                m_Backgrounds.Add(Background);
            }
        }

        /// <summary>
        /// Creates a UIButton instance, and adds it to this UIScreen instance's list of UIButtons.
        /// This function is called from Lua.
        /// </summary>
        /// <param name="ID">The ID of the graphic used to display the button.</param>
        /// <param name="X">The X position of the button.</param>
        /// <param name="Y">The Y position of the button.</param>
        /// <param name="Alpha">The masking color for the button's graphic.</param>
        /// <param name="StrID">The button's string ID.</param>
        public UIButton CreateButton(uint id_0, uint id_1, int X, int Y, int Alpha, string StrID)
        {
            ulong ID = (ulong)(((ulong)id_0) << 32 | ((ulong)(id_1 >> 32)));
            MemoryStream TextureStream;
            Texture2D Texture;

            try
            {
                TextureStream = new MemoryStream(ContentManager.GetResourceFromLongID(ID));
                Texture = Texture2D.FromFile(m_ScreenMgr.GraphicsDevice, TextureStream/*, TCP*/);
            }
            catch (FAR3Exception)
            {
                TextureStream = new MemoryStream(ContentManager.GetResourceFromLongID(ID));
                Texture = Texture2D.FromFile(m_ScreenMgr.GraphicsDevice, TextureStream);
                TextureStream.Close();
            }

            //Why did some genius at Maxis decide it was 'ok' to operate with three masking colors?!!
            if (Alpha == 1)
                ManualTextureMask(ref Texture, new Color(255, 0, 255));
            else if (Alpha == 2)
                ManualTextureMask(ref Texture, new Color(254, 2, 254));
            else if (Alpha == 3)
                ManualTextureMask(ref Texture, new Color(255, 1, 255));

            UIButton btn = new UIButton(X, Y, Texture, StrID, this);

            m_UIElements.Add(btn);

            return btn;
        }

        public void Add(UIElement toAdd)
        {
            m_UIElements.Add(toAdd);
        }

        public UICollectionViewer CreateHeadCatalogBrowser(int X, int Y, string strID)
        {
            //0x100000010  ./avatardata/heads/collections/ea_male_heads.col
            //0x200000010  ./avatardata/heads/collections/ea_female_heads.col
            UICollectionViewer viewer = new UICollectionViewer(X, Y, 39, 44, 6, 8, 33, 33, 2, 2, 3, 7, 0x100000010, 0x200000010, this, strID, m_ScreenMgr);

            m_UIElements.Add(viewer);

            return viewer;
        }

        public UICollectionViewerOutfits CreateBodyCatalogBrowser(int X, int Y, string strID)
        {
	        //0x1d00000010  ./avatardata/bodies/collections/ea_male.col
	        //0x1e00000010  ./avatardata/bodies/collections/ea_female.col
            UICollectionViewerOutfits viewer = new UICollectionViewerOutfits(X, Y, 39, 78, 6, 6, 33, 70, 2, 2, 2, 8, 0x1d00000010, 0x1e00000010, this, strID, m_ScreenMgr);

            m_UIElements.Add(viewer);

            return viewer;
        }

        /// <summary>
        /// Creates a UILabel instance and adds it to this UIScreen's list of UILabels.
        /// This function is called from Lua.
        /// </summary>
        /// <param name="CaptionID">The ID of the caption for the label.</param>
        /// <param name="StrID">The string ID of the label.</param>
        /// <param name="X">The X position of the label.</param>
        /// <param name="Y">The Y position of the label.</param>
        public void CreateLabel(int CaptionID, string StrID, int X, int Y)
        {
            m_UIElements.Add(new UILabel(CaptionID, StrID, X, Y, this));
        }

        /// <summary>
        /// Updates the caption of a specific label.
        /// This function is called from Lua.
        /// </summary>
        /// <param name="StrID">The string ID of the label.</param>
        /// <param name="Caption">The new caption of the label.</param>
        public void UpdateLabel(string StrID, string Caption)
        {
            for(int i = 0; i < m_UIElements.Count; i++)
            {
                if (StrID == m_UIElements[i].StrID)
                {
                    //Not really sure why this works...
                    //The variable Label seems to act like a pointer to m_UIElements[i].
                    UILabel Label = (UILabel)m_UIElements[i];
                    Label.Caption = Caption;
                }
            }
        }

        public void CreateImage(uint id_0, uint id_1, int X, int Y, int Alpha, string StrID)
        {
            ulong ID = (ulong)(((ulong)id_0) << 32 | ((ulong)(id_1 >> 32)));
            MemoryStream TextureStream = new MemoryStream(ContentManager.GetResourceFromLongID(ID));
            Texture2D Texture = Texture2D.FromFile(m_ScreenMgr.GraphicsDevice, TextureStream);

            if (Alpha == 1)
                ManualTextureMask(ref Texture, new Color(255, 0, 255));
            else if (Alpha == 2)
                ManualTextureMask(ref Texture, new Color(254, 2, 254));

            m_UIElements.Add(new UIImage(X, Y, StrID, Texture, this));
        }

        public void CreateTextEdit(int X, int Y, int Width, int Height, int Capacity)
        {
            m_UIElements.Add(new UITextEdit(X, Y, Width, Height, Capacity, "", this));
        }

        public void CreateInfoPopup(int X, int Y, int ID, string Filename, int TextID)
        {
            ImgInfoPopup Popup = new ImgInfoPopup(X, Y, ID, Filename, TextID, this);
            m_Popups.Add(Popup);
        }

        public void CreateLoginDialog(int X, int Y)
        {
            //Note: This can only be loaded when m_Archive has been initialized to load the archive
            //      'dialogs.dat'. This happens only on the first loginscreen (see 'login.lua').
            MemoryStream TexStream = new MemoryStream(ContentManager.GetResourceFromLongID(0xe500000002));
            Texture2D DiagTexture = Texture2D.FromFile(m_ScreenMgr.GraphicsDevice, TexStream);

            string IP = GlobalSettings.Default.LoginServerIP;
            int Port = GlobalSettings.Default.LoginServerPort;

            UILoginDialog LoginDiag = new UILoginDialog(IP, Port, X, Y, DiagTexture, this, "LoginDialog");
            m_NetUIElements.Add(LoginDiag);
        }

        public void CreateMsgBox(int X, int Y, string Message)
        {
            UIMessageBox MsgBox = new UIMessageBox(X, Y, Message, this, "MsgBox");

            m_UIElements.Add(MsgBox);
        }

        #endregion

        public void RemoveInfoPopup(int ID)
        {
            for (int i = 0; i < m_Popups.Count; i++)
            {
                if (ID == m_Popups[i].ID)
                    m_Popups.Remove(m_Popups[i]);
            }
        }

        public virtual void Update(GameTime GTime)
        {
            LuaInterfaceManager.CallFunction("Update");

            //TODO: Check if a button is clicked, and call the Lua function ButtonHandler()
            m_CurrentMouseState = Mouse.GetState();

            foreach (UIElement Element in m_UIElements)
            {
                Element.Update(GTime, ref m_CurrentMouseState, ref m_PreviousMouseState);
                Element.Update(GTime);
            }

            foreach (NetworkedUIElement Element in m_NetUIElements)
            {
                Element.Update(GTime, ref m_CurrentMouseState, ref m_PreviousMouseState);
                Element.Update(GTime);
            }

            for (int i = 0; i < m_Popups.Count; i++ )
                m_Popups[i].Update(GTime, ref m_CurrentMouseState, ref m_PreviousMouseState);

            m_PreviousMouseState = Mouse.GetState();
        }

        public virtual void Draw(SpriteBatch SBatch)
        {
            //lock (m_Backgrounds)
            //{
                foreach (Texture2D Background in m_Backgrounds)
                {
                    if (Background != null)
                    {
                        //Usually a screen only has 1 background, it should be drawn at 0, 0...
                        SBatch.Draw(Background, new Rectangle(0, 0, Background.Width, Background.Height),
                            Color.White);
                    }
                }
            //}

            //Networked UI elements are usually dialogs, so for now it is
            //relatively safe to assume they can be drawn behind other elements.
            foreach (NetworkedUIElement Element in m_NetUIElements)
                Element.Draw(SBatch);

            //lock (m_UIElements)
            //{
                foreach (UIElement Element in m_UIElements)
                {
                    if (Element.DrawingLevel == DrawLevel.DontGiveAFuck)
                        Element.Draw(SBatch);
                }

                foreach (UIElement Element in m_UIElements)
                {
                    if (Element.DrawingLevel == DrawLevel.AlwaysOnTop)
                        Element.Draw(SBatch);
                }
            //}

            for (int i = 0; i < m_Popups.Count; i++)
                m_Popups[i].Draw(SBatch);
        }

        /// <summary>
        /// Manually replaces a specified color in a texture with transparent black,
        /// thereby masking it.
        /// </summary>
        /// <param name="Texture">The texture on which to apply the mask.</param>
        /// <param name="ColorFrom">The color to mask away.</param>
        private void ManualTextureMask(ref Texture2D Texture, Color ColorFrom)
        {
            Color ColorTo = Color.TransparentBlack;

            Color[] data = new Color[Texture.Width * Texture.Height];
            Texture.GetData(data);

            for (int i = 0; i < data.Length; i++)
            {
                data[i].A = 255;
                if (data[i] == ColorFrom)
                    data[i] = ColorTo;
            }

            if (Texture.Format != SurfaceFormat.Color)
                Texture = new Texture2D(Texture.GraphicsDevice, Texture.Width, Texture.Height, 4, TextureUsage.Linear, SurfaceFormat.Color);

            Texture.SetData(data);
        }
    }
}
