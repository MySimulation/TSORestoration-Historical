﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NAudio.Wave;
using Un4seen.Bass;

namespace TSOClient.LUI
{
    public delegate void ButtonClickDelegate();

    /// <summary>
    /// A drawable, clickable button that is part of the GUI.
    /// </summary>
    public class UIButton : UIElement
    {
        private int m_X, m_Y, m_ScaleX, m_ScaleY, m_CurrentFrame;
        private Texture2D m_Texture;
        private string m_StrID;
        private int m_Width;
        private bool m_Disabled;

        private IWavePlayer m_WaveOutDevice = new DirectSoundOut();

        private bool m_Clicking = false;

        public event ButtonClickDelegate OnButtonClick;

        public bool Disabled
        {
            get { return m_Disabled; }
            set { m_Disabled = value; }
        }

        /// <summary>
        /// Gets or sets the x-coordinate for where to render this button.
        /// </summary>
        public int X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        /// <summary>
        /// Gets or sets the y-coordinate for where to render this button.
        /// </summary>
        public int Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }

        public int CurrentFrame
        {
            get
            {
                if (m_CurrentFrame == 0)
                    return 0;
                else
                    return m_Texture.Width / 4 * m_CurrentFrame;
            }

            set
            {
                //Frames go from 0 to 3.
                if(value < 4)
                {
                    m_CurrentFrame = value;
                }
            }
        }

        public UIButton (int X, int Y, Texture2D Texture, string StrID, UIScreen Screen)
            : base(Screen, StrID, DrawLevel.DontGiveAFuck)
        {
            m_X = X;
            m_Y = Y;
            m_Texture = Texture;
            m_StrID = StrID;
            //All buttons have 4 frames...
            m_Width = Texture.Width / 4;
            m_CurrentFrame = 0;
        }

        public UIButton(int X, int Y, int ScaleX, int ScaleY, Texture2D Texture, string StrID, UIScreen Screen)
            : base(Screen, StrID, DrawLevel.DontGiveAFuck)
        {
            m_X = X;
            m_Y = Y;
            m_ScaleX = ScaleX;
            m_ScaleY = ScaleY;
            m_Texture = Texture;
            m_StrID = StrID;
            //All buttons have 4 frames...
            m_Width = Texture.Width / 4;
            m_CurrentFrame = 0;
        }

        public override void Update(GameTime GTime, ref MouseState CurrentMouseState, 
            ref MouseState PrevioMouseState)
        {
            base.Update(GTime, ref CurrentMouseState, ref PrevioMouseState);

            if (!Disabled)
            {
                if (CurrentMouseState.X >= m_X && CurrentMouseState.X <= (m_X + (m_Width + m_ScaleX)) &&
                    CurrentMouseState.Y > m_Y && CurrentMouseState.Y < (m_Y + (m_Texture.Height + m_ScaleY)))
                {
                    if (!m_Clicking)
                        CurrentFrame = 2;

                    if (CurrentMouseState.LeftButton == ButtonState.Pressed &&
                        PrevioMouseState.LeftButton == ButtonState.Released)
                    {
                        m_Clicking = true;
                        //Setting this to 1 seems to cause the animation to be somewhat glitchy,
                        //and I haven't been able to figure out why.
                        CurrentFrame = 0;


                        Bass.BASS_ChannelPlay(UISounds.GetSound(0x01).ThisChannel, false);

                        LuaInterfaceManager.CallFunction("ButtonHandler", this);

                        //This event ususally won't be subscribed to,
                        //it is only used by dialogs that creates buttons
                        //and wants to handle them internally.
                        if (OnButtonClick != null)
                            OnButtonClick();
                    }
                    else
                        m_Clicking = false;
                }
                else
                {
                    m_Clicking = false;
                    CurrentFrame = 0;
                }
            }
            else
            {
                CurrentFrame = 3;
            }
        }

        public override void Draw(SpriteBatch SBatch)
        {
            base.Draw(SBatch);

            if (m_ScaleX == 0 && m_ScaleY == 0)
            {
                //WARNING: Do NOT refer to m_CurrentFrame, as the accessor ensures the right
                //value is returned.
                SBatch.Draw(m_Texture, new Vector2(m_X, m_Y),
                    new Rectangle(CurrentFrame, 0, m_Width, m_Texture.Height), Color.White);
            }
            else
            {
                //WARNING: Do NOT refer to m_CurrentFrame, as the accessor ensures the right
                //value is returned.
                SBatch.Draw(m_Texture, new Rectangle(m_X, m_Y, (m_Width + m_ScaleX), m_Texture.Height + 
                    m_ScaleY), new Rectangle(CurrentFrame, 0, m_Width, m_Texture.Height), Color.White);
            }
        }
    }
}
