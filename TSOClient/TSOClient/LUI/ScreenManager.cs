﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TSOClient.LUI;

namespace TSOClient.LUI
{
    public class ScreenManager
    {
        private Game m_G;
        private List<UIScreen> m_Screens = new List<UIScreen>();
        private SpriteFont m_SprFontBig;
        private SpriteFont m_SprFontSmall;

        private Dictionary<int, string> m_TextDict;

        public Game GameComponent
        {
            get { return m_G; }
        }

        public GraphicsDevice GraphicsDevice
        {
            get { return m_G.GraphicsDevice; }
        }

        public SpriteFont SprFontBig
        {
            get { return m_SprFontBig; }
        }

        public SpriteFont SprFontSmall
        {
            get { return m_SprFontSmall; }
        }

        public UIScreen CurrentScreen
        {
            get { return m_Screens[0]; }
        }

        /// <summary>
        /// Gets or sets the internal dictionary containing all the strings for the game.
        /// </summary>
        public Dictionary<int, string> TextDict
        {
            get { return m_TextDict; }
            set { m_TextDict = value; }
        }

        public ScreenManager(Game G, SpriteFont SprFontBig, SpriteFont SprFontSmall)
        {
            m_G = G;
            m_SprFontBig = SprFontBig;
            m_SprFontSmall = SprFontSmall;
        }

        /// <summary>
        /// Adds a UIScreen instance to this ScreenManager's list of screens.
        /// This function is called from Lua.
        /// </summary>
        /// <param name="Screen">The UIScreen instance to be added.</param>
        public void AddScreen(UIScreen Screen, string LuaPath)
        {
            if (LuaPath != "")
            {
                m_Screens.Add(Screen);
                LuaInterfaceManager.RunFileInThread(LuaPath);
            }
            else
                m_Screens.Add(Screen);
        }

        public void RemoveScreen(UIScreen Screen)
        {
            m_Screens.Remove(Screen);
        }

        /// <summary>
        /// Runs the Lua function that creates the initial UIScreen.
        /// </summary>
        /// <param name="Path">The path to the Lua script containing the function.</param>
        public void LoadInitialScreen(string Path)
        {
            LuaInterfaceManager.RunFileInThread(Path);
        }

        public void Update(GameTime GTime)
        {
            for(int i = 0; i < m_Screens.Count; i++)
                m_Screens[i].Update(GTime);
        }

        public void Draw(SpriteBatch SBatch, float FPS)
        {
            for (int i = 0; i < m_Screens.Count; i++)
            {
                m_Screens[i].Draw(SBatch);
            }

            SBatch.DrawString(m_SprFontBig, "FPS: " + FPS.ToString(), new Vector2(0, 0), Color.Red);
        }
    }
}
