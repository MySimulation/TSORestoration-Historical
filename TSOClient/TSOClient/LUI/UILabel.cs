﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TSOClient.LUI
{
    /// <summary>
    /// A drawable label containing text.
    /// </summary>
    public class UILabel : UIElement
    {
        private int m_X, m_Y;

        private string m_Text = "";
        private string m_StrID = "";

        public string Caption
        {
            get { return m_Text; }
            set { m_Text = value; }
        }

        public int X
        {
            get { return m_X; }
        }

        public int Y
        {
            get { return m_Y; }
        }

        public UILabel(int CaptionID, string StrID, int X, int Y, UIScreen Screen)
            : base(Screen, StrID, DrawLevel.DontGiveAFuck)
        {
            m_X = X;
            m_Y = Y;

            if (Screen.ScreenMgr.TextDict.ContainsKey(CaptionID))
                m_Text = Screen.ScreenMgr.TextDict[CaptionID];
            m_StrID = StrID;
        }

        public override void Draw(SpriteBatch SBatch)
        {
            base.Draw(SBatch);

            SBatch.DrawString(m_Screen.ScreenMgr.SprFontBig, m_Text, new Vector2(m_X, m_Y), Color.Wheat);
        }
    }
}
