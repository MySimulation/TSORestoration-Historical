﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SimsLib.FAR3;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TSOClient.Network;
using TSOClient.Network.Encryption;

namespace TSOClient.LUI
{
    /// <summary>
    /// A clickable, dragable dialog used for logging in to the login server.
    /// </summary>
    public class UILoginDialog : NetworkedUIElement
    {
        private FAR3Archive m_Archive;
        private int m_X, m_Y;
        //The texture for the dialog itself.
        private Texture2D m_DiagImg;
        //The texture for the login progress dialog.
        private Texture2D m_LoginProgressDiag;

        //The caption for the Login dialog.
        private string m_Caption;

        private UILabel m_LblOverallProgress, m_LblCurrentTask;
        private UIProgressBar m_OverallProgressbar, m_CurrentTaskbar;

        private UILabel m_LblAccName, m_LblPass;
        private UITextbox m_TxtAccName, m_TxtPass;

        private UILabel m_LblLogin, m_LblExit;
        private UIButton m_BtnLogin, m_BtnExit;

        /// <summary>
        /// Creates a new instance of LoginDialog. Only used once in the game, during login.
        /// </summary>
        /// <param name="X">X position.</param>
        /// <param name="Y">Y position.</param>
        /// <param name="DiagBackgrnd">The background-texture for the dialog. Loaded from dialogs.dat.</param>
        public UILoginDialog(string IP, int Port, int X, int Y, Texture2D DiagBackgrnd, UIScreen Screen, 
            string StrID) : base(IP, Port, Screen, StrID)
        {
            m_Archive = new FAR3Archive(GlobalSettings.Default.StartupPath + "uigraphics\\cpanel\\cpanel.dat");
            m_DiagImg = DiagBackgrnd;
            m_LoginProgressDiag = DiagBackgrnd;
            m_X = X;
            m_Y = Y;

            //This might have to be passed in to the constructor for language purposes.
            m_Caption = "Login to The Sims Online";

            m_LblAccName = new UILabel(1, "LblAccName", (m_X + 20), (m_Y + 65), Screen);
            m_LblPass = new UILabel(2, "LblPass", (m_X + 20), (m_Y + 125), Screen);
            m_TxtAccName = new UITextbox(0x7A4, (m_X + 20), m_Y + (m_DiagImg.Height / 2),
                1000, 205, Screen, "TxtAccName");
            m_TxtPass = new UITextbox(0x7A4, (m_X + 20), m_Y + ((m_DiagImg.Height / 2) + 60),
                1000, 205, Screen, "TxtPass");

            MemoryStream TexStream = new MemoryStream(m_Archive.GetItemByID(0x1E7));
            Texture2D BtnTex = Texture2D.FromFile(Screen.ScreenMgr.GraphicsDevice, TexStream);
            Color c = new Color(255, 0, 255, 0);
            ManualTextureMask(ref BtnTex, new Color(255, 0, 255, 255));

            //The textures for the progressbars are in 'dialogs.dat'...
            m_Archive = new FAR3Archive(GlobalSettings.Default.StartupPath + "uigraphics\\dialogs\\dialogs.dat");

            TexStream = new MemoryStream(m_Archive.GetItemByID(0x7A5));
            Texture2D ProgressBTex = Texture2D.FromFile(Screen.ScreenMgr.GraphicsDevice, TexStream);

            m_LblOverallProgress = new UILabel(5, "LblOverallProgress", (m_X + m_DiagImg.Width) + 50,
                (m_Y + m_DiagImg.Height) + 150, Screen);
            m_LblCurrentTask = new UILabel(6, "LblCurrentTask", (m_X + m_DiagImg.Width) + 50,
                (m_Y + m_DiagImg.Height) + 208, Screen);

            //Progressbars for showing the loginprocess to the user.
            m_OverallProgressbar = new UIProgressBar((m_X + m_DiagImg.Width) + 50,
                (m_Y + m_DiagImg.Height) + 180, 800, ProgressBTex, "0%", Screen, "OverallProgressBar");
            m_CurrentTaskbar = new UIProgressBar((m_X + m_DiagImg.Width) + 50,
                (m_Y + m_DiagImg.Height) + 238, 800, ProgressBTex, 
                "Authorizing. Prompting for name and password...", Screen, "CurrentTaskBar");

            //These buttons don't have icons, so they need to have text.
            //To avoid making another optional parameter for the UIButton constructor, we
            //can cheat and make the text with UILabels instead.
            m_LblLogin = new UILabel(3, "LblLogin", (m_X + 140), (m_Y + m_DiagImg.Height) + 50, Screen);
            m_LblExit = new UILabel(4, "LblExit", (m_X + m_DiagImg.Width) + 108, (m_Y + m_DiagImg.Height) + 50,
                Screen);

            m_BtnLogin = new UIButton((m_X + 130), (m_Y + m_DiagImg.Height) + 40, 20, 10,
                BtnTex, "BtnLogin", Screen);
            m_BtnExit = new UIButton((m_X + m_DiagImg.Width) + 80, (m_Y + m_DiagImg.Height) + 40, 20, 10,
                BtnTex, "BtnExit", Screen);

            //All classes inheriting from NetworkedUIElement MUST subscribe to these events!
            m_Client.OnNetworkError += new TSOClient.Network.NetworkErrorDelegate(m_Client_OnNetworkError);
            m_Client.OnReceivedData += new TSOClient.Network.ReceivedPacketDelegate(m_Client_OnReceivedData);
            m_BtnLogin.OnButtonClick += new ButtonClickDelegate(m_BtnLogin_ButtonClickEvent);
        }

        /// <summary>
        /// Called when the NetworkClient received a new packet.
        /// </summary>
        /// <param name="Packet">The packet received.</param>
        private void m_Client_OnReceivedData(PacketStream Packet)
        {
            byte Opcode;
            byte Length;

            switch (Packet.PacketID)
            {
                //InitLoginNotify - 21 bytes
                case 0x01:
                    m_OverallProgressbar.UpdateStatus("25%");
                    m_CurrentTaskbar.UpdateStatus("Attempting authorization...");

                    Opcode = (byte)Packet.ReadByte();

                    byte[] SaltBuf = new byte[32];
                    Packet.Read(SaltBuf, 0, 32);
                    m_Client.ClientEncryptor.SRP.Salt = new BigInteger(SaltBuf);

                    byte[] PubEphemeralValueBBuf = new byte[32];
                    Packet.Read(PubEphemeralValueBBuf, 0, 32);
                    m_Client.ClientEncryptor.SRP.PublicEphemeralValueB = new BigInteger(PubEphemeralValueBBuf);

                    m_Client.SendInitLoginNotify(m_Client.ClientEncryptor.SRP.
                        ClientSessionKeyProof.GetBytes(32));
                    break;
                //LoginFailResponse - 2 bytes
                case 0x02:
                    Opcode = (byte)Packet.ReadByte();

                    switch(Packet.ReadByte())
                    {
                        case 0x01:
                            m_Screen.CreateMsgBox(250, 200, "Invalid accountname!");
                            break;
                    }

                    m_Client.Disconnect();
                    break;
                //LoginSuccessResponse - 11 bytes
                case 0x04:
                    m_OverallProgressbar.UpdateStatus("50%");
                    m_CurrentTaskbar.UpdateStatus("Success!");

                    if (!Directory.Exists("CharacterCache"))
                    {
                        Directory.CreateDirectory("CharacterCache");

                        //The charactercache didn't exist, so send the current time, which is
                        //newer than the server's stamp. This will cause the server to send the entire cache.
                        m_Client.SendCharacterInfoRequest(DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss"));
                    }
                    else
                    {
                        //TODO: Check for relevant files within the "CharacterCache" directory...
                    }

                    LuaInterfaceManager.CallFunction("LoginSuccess");
                    break;
                //CharacterInfoResponse
                case 0x05:
                    Opcode = (byte)Packet.ReadByte();
                    Length = (byte)Packet.ReadByte();

                    Packet.DecryptStream(m_Client.ClientEncryptor.SRP.SessionKey.GetBytes(32));
                    //TODO: Process packet...

                    break;
            }
        }

        private void initCasScreen()
        {

        }

        /// <summary>
        /// Called if an exception occured when connecting or performing network related tasks.
        /// </summary>
        /// <param name="Exception">The SocketException instance with information about what went wrong.</param>
        private void m_Client_OnNetworkError(System.Net.Sockets.SocketException Exception)
        {
            string ErrorMsg = "";

            //For now, these are hardcoded.
            switch (Exception.ErrorCode)
            {
                case 10060:
                    if (GlobalSettings.Default.CurrentLang == "English")
                        ErrorMsg = "Couldn't connect \n- connection timed out!";
                    else if (GlobalSettings.Default.CurrentLang == "Norwgian")
                        ErrorMsg = "Kunne ikke koble til \n- forbindelsen fikk tidsavbrudd!";
                    break;
                case 10061:
                    if (GlobalSettings.Default.CurrentLang == "English")
                        ErrorMsg = "Couldn't connect \n- connection refused!";
                    else if (GlobalSettings.Default.CurrentLang == "Norwegian")
                        ErrorMsg = "Kunne ikke koble til \n- tilkobling ble nektet!";
                    break;
                case 10064:
                    if (GlobalSettings.Default.CurrentLang == "English")
                        ErrorMsg = "Couldn't connect \n- host is down!";
                    else if (GlobalSettings.Default.CurrentLang == "Norwegian")
                        ErrorMsg = "Kunne ikke koble til \n- tjeneren er nede!";
                    break;
                case 10065:
                    if (GlobalSettings.Default.CurrentLang == "English")
                        ErrorMsg = "Couldn't connect \n- encountered unreachable host!";
                    else if (GlobalSettings.Default.CurrentLang == "Norwegian")
                        ErrorMsg = "Kunne ikke koble til \n- fant ikke tjenerens adresse!";
                    break;
                case 1101:
                    if (GlobalSettings.Default.CurrentLang == "English")
                        ErrorMsg = "Couldn't connect \n- host could not be found!";
                    else if (GlobalSettings.Default.CurrentLang == "Norwegian")
                        ErrorMsg = "Kunne ikke koble til \n- fant ikke tjener!";
                    break;
            }

            //TODO: Create a messagebox here, informing the user what went wrong...
            m_Screen.CreateMsgBox(270, 150, ErrorMsg);
        }

        /// <summary>
        /// Called when the Login button was clicked.
        /// </summary>
        private void m_BtnLogin_ButtonClickEvent()
        {
            ContentManager.InitFloors(m_Screen.ScreenMgr.GraphicsDevice);
            LuaInterfaceManager.CallFunction("LoginSuccess");
            //m_Client.Connect(m_TxtAccName.CurrentText.ToUpper(), m_TxtPass.CurrentText.ToUpper());
        }

        public override void Update(GameTime GTime, ref MouseState CurrentMouseState, 
            ref MouseState PrevioMouseState)
        {
            base.Update(GTime, ref CurrentMouseState, ref PrevioMouseState);

            m_TxtAccName.Update(GTime, ref CurrentMouseState, ref PrevioMouseState);
            m_TxtPass.Update(GTime, ref CurrentMouseState, ref PrevioMouseState);

            //No need to call update on the labels...

            m_BtnLogin.Update(GTime, ref CurrentMouseState, ref PrevioMouseState);
            m_BtnExit.Update(GTime, ref CurrentMouseState, ref PrevioMouseState);

            m_OverallProgressbar.Update(GTime);
            m_CurrentTaskbar.Update(GTime);
        }

        public override void Draw(SpriteBatch SBatch)
        {
            SBatch.Draw(m_DiagImg, new Rectangle(m_X, m_Y, (m_DiagImg.Width + 200), (m_DiagImg.Height + 100)),
                new Color(255, 255, 255, 205));
            SBatch.Draw(m_LoginProgressDiag, new Rectangle((m_X + m_DiagImg.Width), 
                (m_Y + m_DiagImg.Height) + 115, m_LoginProgressDiag.Width + 200,
                m_LoginProgressDiag.Height - 20), new Color(255, 255, 255, 205));

            SBatch.DrawString(m_Screen.ScreenMgr.SprFontBig, m_Caption, new Vector2(m_X + (m_DiagImg.Width / 2),
                m_Y), Color.Wheat);
            SBatch.DrawString(m_Screen.ScreenMgr.SprFontBig, "The Sims Online Login",
                new Vector2((m_X + m_DiagImg.Width) + 110, (m_Y + m_DiagImg.Height) + 115), Color.Wheat);

            m_LblAccName.Draw(SBatch);
            m_LblPass.Draw(SBatch);
            m_TxtAccName.Draw(SBatch);
            m_TxtPass.Draw(SBatch);

            m_BtnLogin.Draw(SBatch);
            m_BtnExit.Draw(SBatch);
            //The labels MUST be drawn ABOVE the buttons...
            m_LblLogin.Draw(SBatch);
            m_LblExit.Draw(SBatch);

            m_LblOverallProgress.Draw(SBatch);
            m_LblCurrentTask.Draw(SBatch);
            m_OverallProgressbar.Draw(SBatch);
            m_CurrentTaskbar.Draw(SBatch);


            base.Draw(SBatch);
        }
    }
}
