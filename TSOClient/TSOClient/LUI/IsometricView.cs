﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.Wave;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Threading;

namespace TSOClient.LUI
{
    public class IsometricView : UIScreen
    {
        private int myX, myY, mySize;
        private bool m_Disabled;

        private IWavePlayer m_WaveOutDevice = new DirectSoundOut();

        private bool m_Clicking = false;

        public event ButtonClickDelegate OnButtonClick;

        private List<GameObject> myGameObjects;

        public bool Disabled
        {
            get { return m_Disabled; }
            set { m_Disabled = value; }
        }

        /// <summary>
        /// Gets or sets the x-coordinate for where to render this button.
        /// </summary>
        public int X
        {
            get { return myX; }
            set { myY = value; }
        }

        /// <summary>
        /// Gets or sets the y-coordinate for where to render this button.
        /// </summary>
        public int Y
        {
            get { return myY; }
            set { myY = value; }
        }

        public int Size
        {
            get { return mySize; }
        }

        public IsometricView (ScreenManager ScreenMgr)
            : base(ScreenMgr)
        {
            myX = 0;
            myY = 0;
            mySize = 12;
            myGameObjects = new List<GameObject>();

            InitFloorsDebug();
        }

        public IsometricView(int Size, ScreenManager ScreenMgr)
            : base(ScreenMgr)
        {
            myX = X;
            myY = Y;
            mySize = Size;
            myGameObjects = new List<GameObject>();

            InitFloorsDebug();
        }

        public void InitFloorsDebug()
        {
            GameObject.DrawSize = 0;
            GameObject.Rotation = 1;
            int x = 0;
            int y = 0;
            foreach (Floor f in ContentManager.Floors)
            {
                if (x > 10)
                {
                    y ++;
                    x = 0;
                }
                PlaceObjectInWorld(f, x++, y);
            }
        }

        public void PlaceObjectInWorld(GameObject obj, int x, int y)
        {
            obj.Position = new int[] { x, y };
            myGameObjects.Add(obj);
        }

        public int MouseX { get { return Mouse.GetState().X; } }
        public int MouseY { get { return Mouse.GetState().Y; } }

        public void MoveLeft()
        {
            GameObject.GlobalXTranslation--;
        }

        public void MoveRight()
        {
            GameObject.GlobalXTranslation++;
        }

        public void MoveUp()
        {
            GameObject.GlobalYTranslation++;
        }

        public void MoveDown()
        {
            GameObject.GlobalYTranslation--;
        }

        static bool lastnight = false;
        static bool lastnight2 = false;
        static bool lastnight3 = false;
        static bool lastnight4 = false;
        public override void Update(GameTime GTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                if (lastnight)
                {
                    GameObject.Rotation++;
                    Thread.Sleep(75);
                }
                lastnight = !lastnight;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                if (lastnight2)
                {
                    GameObject.Rotation--;
                    Thread.Sleep(75);
                }
                lastnight2 = !lastnight2;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Add))
            {
                if (lastnight3)
                {
                    GameObject.DrawSize++;
                    Thread.Sleep(75);
                }
                lastnight3 = !lastnight3;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Subtract))
            {
                if (lastnight4)
                {
                    GameObject.DrawSize--;
                    Thread.Sleep(75);
                }
                lastnight4 = !lastnight4;
            }
            base.Update(GTime);
        }

        public override void Draw(SpriteBatch SBatch)
        {
            foreach (Floor f in ContentManager.Floors)
            {
                f.Draw(SBatch);
            }

            base.Draw(SBatch);
        }
    }
}
