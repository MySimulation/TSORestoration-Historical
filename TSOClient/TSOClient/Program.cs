using System;
using TSOClient.LUI;

namespace TSOClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Game1 game = new Game1())
            {
                //LuaFunctionAttribute.RegisterAllLuaFunctions(game, LuaInterfaceManager.LuaVM);
                game.Run();
            }
        }
    }
}

