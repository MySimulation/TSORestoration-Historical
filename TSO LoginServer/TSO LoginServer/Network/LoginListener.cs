﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace TSO_LoginServer.Network
{
    public delegate void OnReceiveDelegate(PacketStream P, LoginClient Client);

    /// <summary>
    /// Represents a listener that listens for incoming login clients.
    /// </summary>
    public class LoginListener //: Listener
    {
        private ArrayList m_LoginClients;
        private Socket m_ListenerSock;
        private IPEndPoint m_LocalEP;

        public event OnReceiveDelegate OnReceiveEvent;

        public LoginListener()
        {
            m_ListenerSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_LoginClients = ArrayList.Synchronized(new ArrayList());
        }

        public void Initialize(int Port)
        {
            IPEndPoint LocalEP = new IPEndPoint(IPAddress.Any, Port);

            m_LocalEP = LocalEP;

            try
            {
                m_ListenerSock.Bind(LocalEP);
                m_ListenerSock.Listen(10000);

                Console.WriteLine("Started listening on: " + LocalEP.Address.ToString()
                    + ":" + LocalEP.Port);
            }
            catch (SocketException E)
            {
                Console.WriteLine("Winsock error caused by call to Socket.Bind(): \n" + E.ToString());
            }

            m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
        }

        public void OnAccept(IAsyncResult AR)
        {
            Socket AcceptedSocket = m_ListenerSock.EndAccept(AR);

            if (AcceptedSocket != null)
            {
                Console.WriteLine("\nNew client connected!");

                //Let sockets linger for 5 seconds after they're closed, in an attempt to make sure all
                //pending data is sent!
                AcceptedSocket.LingerState = new LingerOption(true, 5);
                LoginClient NewClient = new LoginClient(AcceptedSocket, this);
                m_LoginClients.Add(NewClient);
            }

            m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
        }

        /// <summary>
        /// Called by LoginClient instances
        /// when they've received some new data
        /// (a new packet). Should not be called
        /// from anywhere else.
        /// </summary>
        /// <param name="P"></param>
        public void OnReceivedData(PacketStream P, LoginClient Client)
        {
            OnReceiveEvent(P, Client);
        }

        /// <summary>
        /// Removes a client from the internal list of connected clients.
        /// Should really only be called internally by the LoginClient.Disconnect()
        /// method.
        /// </summary>
        /// <param name="Client">The client to remove.</param>
        public void RemoveClient(LoginClient Client)
        {
            m_LoginClients.Remove(Client);
        }

        /// <summary>
        /// The number of clients that are connected to this LoginListener instance.
        /// </summary>
        public int NumConnectedClients
        {
            get { return m_LoginClients.Count; }
        }
    }
}
