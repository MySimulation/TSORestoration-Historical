﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using TSO_LoginServer.Network.Encryption;

namespace TSO_LoginServer.Network
{
    /// <summary>
    /// An object used to pass other objects asynchronously to a callback function.
    /// </summary>
    class DBAsyncObject
    {
        private LoginClient m_Client;
        private PatchClient m_PServer;
        private SqlCommand m_Command;
        private BigInteger m_PublicEphemeralValA;
        private string m_AccountName, m_Password;

        //IDs corresponding to unique characters in the DB.
        public int CharacterID1, CharacterID2, CharacterID3;

        public DBAsyncObject(string AccountName, string Password, ref LoginClient Client, 
            PatchClient PServer, SqlCommand Command)
        {
            m_Client = Client;
            m_PServer = PServer;
            m_Command = Command;
            m_AccountName = AccountName;
            m_Password = Password;
        }

        public DBAsyncObject(string AccountName, ref LoginClient Client, SqlCommand Command, 
            ref BigInteger PubEphemeralValA)
        {
            m_Client = Client;
            m_AccountName = AccountName;
            m_Command = Command;
            m_PublicEphemeralValA = PubEphemeralValA;
        }

        /// <summary>
        /// Creates a new DBAsynchObject.
        /// </summary>
        /// <param name="AccountName">The name of the client's account.</param>
        /// <param name="Client">The client.</param>
        /// <param name="Command">The SQL command.</param>
        public DBAsyncObject(string AccountName, LoginClient Client, SqlCommand Command)
        {
            m_Client = Client;
            m_AccountName = AccountName;
            m_Command = Command;
        }

        public string AccountName
        {
            get { return m_AccountName; }
        }

        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }

        public LoginClient Client
        {
            get { return m_Client; }
        }

        public PatchClient PServer
        {
            get { return m_PServer; }
        }

        public SqlCommand Cmd
        {
            get { return m_Command; }
        }

        public BigInteger PublicEphemeralValueA
        {
            get { return m_PublicEphemeralValA; }
        }
    }

    class Database
    {
        private static SqlConnection m_Connection;

        public static void Connect()
        {
            try
            {
                m_Connection = new SqlConnection("Data Source=Afr0-PC\\SQLEXPRESS;" + 
                    "Initial Catalog=TSO;User Id=Afr0;Password=Prins123;Asynchronous Processing=true");
                m_Connection.Open();
            }
            catch (Exception)
            {
                throw new NoDBConnection("Couldn't connect to database server! Reverting to flat file DB.");
            }
        }

        /// <summary>
        /// Checks for the existence of an account in the database and sends a packet to a client
        /// telling it whether or not the account existed.
        /// </summary>
        /// <param name="AccountName">The accountname to check for.</param>
        /// <param name="Password">The password to check for.</param>
        /// <param name="Client">The client to send the packet to.</param>
        /// <param name="PServer">The patchserver to send a key to (if the login was successful).</param>
        public static void CheckAccount(string AccountName, string Password, LoginClient Client, 
            PatchClient PServer)
        {
            if (m_Connection == null)
            {
                if (GlobalSettings.Default.CreateAccountsOnLogin == false)
                {
                    //TODO: Check if a flat file database exists, otherwise send an accountlogin failed packet.
                }
                else
                {
                    //TODO: Write account into flat file DB if it doesn't exist.
                }
            }

            //Gets the data from both rows (AccountName & Password) 
            SqlCommand Command = new SqlCommand("SELECT AccountName, Password FROM Accounts");
            Command.Connection = m_Connection;
            Command.BeginExecuteReader(new AsyncCallback(EndCheckAccountName), 
                new DBAsyncObject(AccountName, Password, ref Client, PServer, Command));
        }

        /// <summary>
        /// Checks for the existence of an account in the database and sends a packet to a client
        /// telling it whether or not the account existed.
        /// </summary>
        /// <param name="AccountName">The accountname to check for.</param>
        /// <param name="Client">The client to send the packet to.</param>
        /// <param name="PublicEphemeralValA">The PublicEphemeralValueA sent from the client.</param>
        public static void CheckAccount(string AccountName, LoginClient Client, BigInteger PublicEphemeralValA)
        {
            if (m_Connection == null)
            {
                if (GlobalSettings.Default.CreateAccountsOnLogin == false)
                {
                    //TODO: Check if a flat file database exists, otherwise send an accountlogin failed packet.
                }
                else
                {
                    //TODO: Write account into flat file DB if it doesn't exist.
                }
            }

            //Gets the data from both rows (AccountName & Password) 
            SqlCommand Command = new SqlCommand("SELECT AccountName, Password FROM Accounts");
            Command.Connection = m_Connection;
            Command.BeginExecuteReader(new AsyncCallback(EndCheckAccountName),
                new DBAsyncObject(AccountName, ref Client, Command, ref PublicEphemeralValA));
        }

        /// <summary>
        /// Checks when a client's characters were last cached, against a timestamp received from the client.
        /// If the client's timestamp doesn't match the one in the DB (meaning it was older or newer), information
        /// about all the characters is sent to the client.
        /// </summary>
        /// <param name="Timestamp">The timestamp received from the client.</param>
        public static void CheckCharacterTimestamp(string AccountName, LoginClient Client, DateTime Timestamp)
        {
            SqlCommand Command = new SqlCommand("SELECT Character1, Character2, Character3 FROM Accounts");
            Command.Connection = m_Connection;
            Command.BeginExecuteReader(new AsyncCallback(EndCheckCharacterID),
                new DBAsyncObject(AccountName, Client, Command));
        }

        /// <summary>
        /// Creates a character in the DB.
        /// </summary>
        /// <param name="TimeStamp">When the character was last cached, should be equal to DateTime.Now</param>
        /// <param name="Name">The name of the character.</param>
        /// <param name="Sex">The sex of the character, should be MALE or FEMALE.</param>
        public static void CreateCharacter(string TimeStamp, string Name, string Sex)
        {
            SqlCommand Command = new SqlCommand("INSERT INTO Character(LastCached, Name, Sex) VALUES('" +
                TimeStamp + "', '" + Name + "', '" + Sex  + "')");
            Command.BeginExecuteNonQuery(new AsyncCallback(EndCreateCharacter), Command);
        }

        /// <summary>
        /// Creates an account in the DB.
        /// </summary>
        /// <param name="AccountName">The accountname.</param>
        /// <param name="Password">The password.</param>
        public static void CreateAccount(string AccountName, string Password)
        {
            SqlCommand Command = new SqlCommand("INSERT INTO Accounts(AccountName, Password) VALUES('" +
                AccountName + "', '" + Password + "')");
            Command.BeginExecuteNonQuery(new AsyncCallback(EndCreateAccount), Command);
        }

        private static void EndCheckAccountName(IAsyncResult AR)
        {
            DBAsyncObject AsyncObject = AR.AsyncState as DBAsyncObject;
            bool FoundAccountName = false;
            bool FoundPassword = false;

            using (SqlDataReader Reader = AsyncObject.Cmd.EndExecuteReader(AR))
            {
                while (Reader.Read())
                {
                    if ((string)Reader[0] == AsyncObject.AccountName)
                    {
                        FoundAccountName = true;

                        AsyncObject.Password = (string)Reader[1];
                    }
                }
            }

            if (FoundAccountName == true /*&& FoundPassword == true*/)
            {
                //0x01 = InitLoginNotify
                PacketStream P = new PacketStream(0x01, 65);

                //TODO: Make this threadsafe...
                AsyncObject.Client.ClientEncryptor = new Encryptor(AsyncObject.AccountName.ToUpper(),
                    AsyncObject.Password.ToUpper());
                AsyncObject.Client.ClientEncryptor.SRP.PublicEphemeralValueA = AsyncObject.PublicEphemeralValueA;

                P.WriteByte(0x01);
                P.Write(AsyncObject.Client.ClientEncryptor.SRP.Salt.GetBytes(32), 0, 32);
                P.Write(AsyncObject.Client.ClientEncryptor.SRP.PublicEphemeralValueB.GetBytes(32), 0, 32);

                AsyncObject.Client.Send(P.ToArray());

                Logger.LogInfo("Sent InitLoginNotify!\r\n");
            }
            else
            {
                PacketStream P = new PacketStream(0x02, 2);
                P.WriteByte(0x02);
                P.WriteByte(0x01);
                AsyncObject.Client.Send(P.ToArray());

                Logger.LogInfo("Bad accountname - sent SLoginFailResponse!\r\n");
                //AsyncObject.Client.Disconnect();
            }

            //If this setting is true, it means an account will be created
            //if it doesn't exist.
            if (GlobalSettings.Default.CreateAccountsOnLogin == false)
            {
                /*if (FoundAccountName == true && FoundPassword == false)
                {
                    //0x82 = Login Failed packet.
                    PacketStream P = new PacketStream(0x82, 2);
                    P.WriteByte(0x82);
                    //Reason = Bad password.
                    P.WriteByte(0x02);

                    AsyncObject.Client.Send(P.ToArray());
                }
                else if (FoundAccountName == false && FoundPassword == true)
                {
                    //0x82 = Login Failed packet.
                    PacketStream P = new PacketStream(0x82, 2);
                    P.WriteByte(0x82);
                    //Reason = Bad accountname.
                    P.WriteByte(0x01);

                    AsyncObject.Client.Send(P.ToArray());
                }
                else if (FoundAccountName == true && FoundPassword == false)
                {
                    //0x82 = Login Failed packet.
                    PacketStream P = new PacketStream(0x82, 2);
                    P.WriteByte(0x82);
                    //Reason = Both! Someone tried to cheat us! :o
                    P.WriteByte(0x03);

                    AsyncObject.Client.Send(P.ToArray());
                }*/
            }
            else
            {
                if (FoundAccountName == false && FoundPassword == false)
                {
                    //No idea if this call is gonna succeed, given it's called from a callback function...
                    CreateAccount(AsyncObject.AccountName, AsyncObject.Password);
                }
            }
        }

        private static void EndCreateAccount(IAsyncResult AR)
        {
            SqlCommand Cmd = AR.AsyncState as SqlCommand;

            Cmd.EndExecuteNonQuery(AR);
        }

        /// <summary>
        /// Callback mehod for CheckCharacterTimestamp.
        /// This queries for the existence of a particular account
        /// in the DB and retrieves the character IDs associated with it.
        /// </summary>
        private static void EndCheckCharacterID(IAsyncResult AR)
        {
            DBAsyncObject AsyncObject = AR.AsyncState as DBAsyncObject;
            bool FoundAccountName = false;

            int CharacterID1 = 0;
            int CharacterID2 = 0;
            int CharacterID3 = 0;

            using (SqlDataReader Reader = AsyncObject.Cmd.EndExecuteReader(AR))
            {
                while (Reader.Read())
                {
                    if ((string)Reader[0] == AsyncObject.AccountName)
                    {
                        FoundAccountName = true;

                        CharacterID1 = (int)Reader[3];
                        CharacterID2 = (int)Reader[4];
                        CharacterID3 = (int)Reader[5];

                        if (FoundAccountName == true)
                            break;
                    }
                }
            }

            if (FoundAccountName)
            {
                SqlCommand Command = new SqlCommand("SELECT CharacterID, LastCached, Name, Sex FROM Character");

                AsyncObject.CharacterID1 = CharacterID1;
                AsyncObject.CharacterID2 = CharacterID2;
                AsyncObject.CharacterID3 = CharacterID3;

                Command.Connection = m_Connection;
                Command.BeginExecuteReader(new AsyncCallback(EndCheckCharacterTimestamp), AsyncObject);
            }
        }

        /// <summary>
        /// Callback method for EndCheckCharacterID.
        /// This retrieves information about the characters 
        /// corresponding to the IDs retrieved earlier.
        /// </summary>
        private static void EndCheckCharacterTimestamp(IAsyncResult AR)
        {
            DBAsyncObject AsyncObject = AR.AsyncState as DBAsyncObject;

            using (SqlDataReader Reader = AsyncObject.Cmd.EndExecuteReader(AR))
            {
                while (Reader.Read())
                {
                    if ((int)Reader[0] == AsyncObject.CharacterID1)
                    {
                        Sim Character = new Sim();
                        Character.CharacterID = AsyncObject.CharacterID1;
                        Character.Timestamp = (string)Reader[1];
                        Character.Name = (string)Reader[2];
                        Character.Sex = (string)Reader[3];
                    }
                    else if ((int)Reader[1] == AsyncObject.CharacterID1)
                    {
                        Sim Character = new Sim();
                        Character.CharacterID = AsyncObject.CharacterID1;
                        Character.Timestamp = (string)Reader[1];
                        Character.Name = (string)Reader[2];
                        Character.Sex = (string)Reader[3];
                    }
                    else if ((int)Reader[2] == AsyncObject.CharacterID1)
                    {
                        Sim Character = new Sim();
                        Character.CharacterID = AsyncObject.CharacterID1;
                        Character.Timestamp = (string)Reader[1];
                        Character.Name = (string)Reader[2];
                        Character.Sex = (string)Reader[3];

                        //For now, assume that finding the third character means
                        //all characters have been found.
                        break;
                    }
                }
            }
        }

        private static void EndCreateCharacter(IAsyncResult AR)
        {
            SqlCommand Cmd = AR.AsyncState as SqlCommand;

            Cmd.EndExecuteNonQuery(AR);
        }
    }
}
