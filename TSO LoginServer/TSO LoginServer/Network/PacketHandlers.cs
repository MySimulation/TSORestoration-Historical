 using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using TSO_LoginServer.Network.Encryption;

namespace TSO_LoginServer.Network
{
    /// <summary>
    /// Contains static methods for handling incoming packets.
    /// </summary>
    class PacketHandlers
    {
        /// <summary>
        /// The patchserver came online and tried to login!
        /// </summary>
        public static void HandlePatchLogin(PacketStream P, PatchClient Client)
        {
            int Port = P.ReadInt32();
            long IP = P.ReadInt64();

            GlobalSettings.Default.PatchServerPort = Port;
            GlobalSettings.Default.PatchServerIP = NetworkHelp.LongToIP(IP);

            MessageBox.Show("PatchServer Port: " + Port.ToString() + 
                "\nPatchServer IP: " + NetworkHelp.LongToIP(IP));
        }

        /// <summary>
        /// A client tried to log in with an account!
        /// </summary>
        /// <param name="P"></param>
        public static void HandleAccountLogin(PacketStream P, LoginClient Client, PatchClient PServer)
        {
            string AccountName = P.ReadString(30);
            string Password = P.ReadString(30);

            Database.CheckAccount(AccountName, Password, Client, PServer);
        }

        public static void HandleLoginRequest(PacketStream P, LoginClient Client)
        {
            Logger.LogInfo("Received LoginRequest!\r\n");

            byte PacketLength = (byte)P.ReadByte();

            byte AccountStrLength = (byte)P.ReadByte();

            byte[] AccountNameBuf = new byte[AccountStrLength];
            P.Read(AccountNameBuf, 0, AccountStrLength);
            string AccountName = Encoding.ASCII.GetString(AccountNameBuf);
            Logger.LogInfo("Accountname: " + AccountName + "\r\n");

            byte[] PublicEphemeralValueABuf = new byte[32];
            P.Read(PublicEphemeralValueABuf, 0, 32);

            byte Version1 = (byte)P.ReadByte();
            byte Version2 = (byte)P.ReadByte();
            byte Version3 = (byte)P.ReadByte();
            byte Version4 = (byte)P.ReadByte();

            Logger.LogInfo("Done reading LoginRequest, checking account...\r\n");

            Database.CheckAccount(AccountName, Client, new BigInteger(PublicEphemeralValueABuf));
        }

        public static void HandleInitLoginNotify(PacketStream P, LoginClient Client)
        {
            byte[] ClientSessionKeyProofBuf = new byte[32];
            P.Read(ClientSessionKeyProofBuf, 0, 32);

            if (Client.ClientEncryptor.SRP.IsClientProofValid(new BigInteger(ClientSessionKeyProofBuf)))
            {
                Logger.LogInfo("Client's proof was valid!\r\n");

                PacketStream LoginSuccessResponsePacket = new PacketStream(0x04, 33);
                LoginSuccessResponsePacket.WriteByte(0x04);
                LoginSuccessResponsePacket.WriteBytes(Client.ClientEncryptor.
                    SRP.ServerSessionKeyProof.GetBytes(32));

                Client.Send(LoginSuccessResponsePacket.ToArray());
            }
        }

        public static void HandleCharacterInfoRequest(PacketStream P, LoginClient Client)
        {
            byte PacketLength = (byte)P.ReadByte();
            P.DecryptStream(Client.ClientEncryptor.SRP.SessionKey.GetBytes(32));

            DateTime TimeStamp = DateTime.Parse(P.ReadString());

            Database.CheckCharacterTimestamp(Client.ClientEncryptor.SRP.Username, Client, TimeStamp);
        }
    }
}
