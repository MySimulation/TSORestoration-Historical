﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSO_LoginServer.Network
{
    /// <summary>
    /// Exception thrown if the server couldn't connect to an MS SQL server.
    /// </summary>
    class NoDBConnection : Exception
    {
        public NoDBConnection(string Message)
            : base(Message)
        {

        }
    }
}
