﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSO_LoginServer
{
    /// <summary>
    /// Represents a Sim/Character in the game.
    /// </summary>
    class Sim
    {
        private int m_CharacterID;
        private string m_Timestamp;
        private string m_Name;
        private string m_Sex;

        /// <summary>
        /// The character's ID, as it exists in the DB.
        /// </summary>
        public int CharacterID
        {
            get { return m_CharacterID; }
            set { m_CharacterID = value; }
        }

        /// <summary>
        /// When was this character last cached by the client?
        /// </summary>
        public string Timestamp
        {
            get { return m_Timestamp; }
            set { m_Timestamp = value; }
        }

        /// <summary>
        /// The character's name, as it exists in the DB.
        /// </summary>
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string Sex
        {
            get { return m_Sex; }
            set { m_Sex = value; }
        }

        public Sim()
        {

        }
    }
}
