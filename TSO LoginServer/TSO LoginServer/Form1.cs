﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TSO_LoginServer.Network;

namespace TSO_LoginServer
{
    public partial class Form1 : Form
    {
        private LoginListener m_Listener;
        private PatchListener m_PatchListener;

        public Form1()
        {
            InitializeComponent();

            try
            {
                Database.Connect();
            }
            catch (NoDBConnection NoDB)
            {
                MessageBox.Show(NoDB.Message);
                Environment.Exit(0);
            }

            Logger.Initialize("Log.txt");
            Logger.InfoEnabled = true;

            m_Listener = new LoginListener();
            m_Listener.OnReceiveEvent += new OnReceiveDelegate(m_Listener_OnReceiveEvent);

            //LoginClient.RegisterLoginPacketID(0x80, 61);
            //LoginRequest - Unknown size.
            LoginClient.RegisterLoginPacketID(0x00, 0);
            //InitLoginNotify - 33 bytes
            LoginClient.RegisterLoginPacketID(0x01, 33);

            m_Listener.Initialize(2106);

            m_PatchListener = new PatchListener();
            m_PatchListener.OnReceiveEvent += new OnPatchReceiveDelegate(m_PatchListener_OnReceiveEvent);

            PatchClient.RegisterPatchPacketID(0x84, 13);

            m_PatchListener.Initialize(1338);
        }

        /// <summary>
        /// Handles incoming packets from the patchserver.
        /// </summary>
        void m_PatchListener_OnReceiveEvent(PacketStream P, PatchClient Client)
        {
            byte ID = (byte)P.ReadByte();

            switch (ID)
            {
                case 0x84:
                    PacketHandlers.HandlePatchLogin(P, Client);
                    break;
            }
        }

        /// <summary>
        /// Handles incoming packets from connected clients.
        /// </summary>
        void m_Listener_OnReceiveEvent(PacketStream P, LoginClient Client)
        {
            byte ID = (byte)P.ReadByte();

            switch (ID)
            {
                /*case 0x80:
                    PacketHandlers.HandleAccountLogin(P, (LoginClient)Client, m_PatchListener.PatchServer);
                    break;*/
                case 0x00:
                    PacketHandlers.HandleLoginRequest(P, Client);
                    break;
                case 0x01:
                    PacketHandlers.HandleInitLoginNotify(P, Client);
                    break;
                case 0x05:
                    PacketHandlers.HandleCharacterInfoRequest(P, Client);
                    break;
                default:
                    Logger.LogInfo("Received unhandled packet - ID: " + P.PacketID);
                    break;
            }
        }
    }
}
