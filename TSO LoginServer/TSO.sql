USE [master]
GO
/****** Object:  Database [TSO]    Script Date: 09/04/2011 01:47:39 ******/
CREATE DATABASE [TSO] ON  PRIMARY 
( NAME = N'TSO', FILENAME = N'c:\Program Files (x86)\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\TSO.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 102400KB )
 LOG ON 
( NAME = N'TSO_log', FILENAME = N'c:\Program Files (x86)\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\TSO_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'TSO', @new_cmptlevel=90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TSO].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TSO] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [TSO] SET ANSI_NULLS OFF
GO
ALTER DATABASE [TSO] SET ANSI_PADDING OFF
GO
ALTER DATABASE [TSO] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [TSO] SET ARITHABORT OFF
GO
ALTER DATABASE [TSO] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [TSO] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [TSO] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [TSO] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [TSO] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [TSO] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [TSO] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [TSO] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [TSO] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [TSO] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [TSO] SET  ENABLE_BROKER
GO
ALTER DATABASE [TSO] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [TSO] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [TSO] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [TSO] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [TSO] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [TSO] SET  READ_WRITE
GO
ALTER DATABASE [TSO] SET RECOVERY SIMPLE
GO
ALTER DATABASE [TSO] SET  MULTI_USER
GO
ALTER DATABASE [TSO] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [TSO] SET DB_CHAINING OFF
GO
USE [TSO]
GO
/****** Object:  User [Mats]    Script Date: 09/04/2011 01:47:39 ******/
CREATE USER [Mats] FOR LOGIN [Afr0] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Character]    Script Date: 09/04/2011 01:47:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Character](
	[CharacterID] [int] IDENTITY(1,1) NOT NULL,
	[LastCached] [varchar](50) NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Character] PRIMARY KEY CLUSTERED 
(
	[CharacterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FTPAccounts]    Script Date: 09/04/2011 01:47:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FTPAccounts](
	[AccountName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 09/04/2011 01:47:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[AccountID] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Character1] [int] NULL,
	[Character2] [int] NULL,
	[Character3] [int] NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Accounts_Character1]    Script Date: 09/04/2011 01:47:39 ******/
ALTER TABLE [dbo].[Accounts]  WITH CHECK ADD  CONSTRAINT [FK_Accounts_Character1] FOREIGN KEY([Character1])
REFERENCES [dbo].[Character] ([CharacterID])
GO
ALTER TABLE [dbo].[Accounts] CHECK CONSTRAINT [FK_Accounts_Character1]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'References the first character for the account.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts', @level2type=N'CONSTRAINT',@level2name=N'FK_Accounts_Character1'
GO
/****** Object:  ForeignKey [FK_Accounts_Character2]    Script Date: 09/04/2011 01:47:39 ******/
ALTER TABLE [dbo].[Accounts]  WITH CHECK ADD  CONSTRAINT [FK_Accounts_Character2] FOREIGN KEY([Character2])
REFERENCES [dbo].[Character] ([CharacterID])
GO
ALTER TABLE [dbo].[Accounts] CHECK CONSTRAINT [FK_Accounts_Character2]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'References the second character for the account.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts', @level2type=N'CONSTRAINT',@level2name=N'FK_Accounts_Character2'
GO
/****** Object:  ForeignKey [FK_Accounts_Character3]    Script Date: 09/04/2011 01:47:39 ******/
ALTER TABLE [dbo].[Accounts]  WITH CHECK ADD  CONSTRAINT [FK_Accounts_Character3] FOREIGN KEY([Character3])
REFERENCES [dbo].[Character] ([CharacterID])
GO
ALTER TABLE [dbo].[Accounts] CHECK CONSTRAINT [FK_Accounts_Character3]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'References the third character for the account.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts', @level2type=N'CONSTRAINT',@level2name=N'FK_Accounts_Character3'
GO
